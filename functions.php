<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//define constants for flash
define('FLASH_SUCCESS', "success");
define('FLASH_ERROR', "error");
define('FLASH_INFO', "info");

function is_live() {
    if (defined("ENVIRONMENT") && ENVIRONMENT == "live") {
        return true;
    }
    return false;
}

function mail_error($title, $content) {
    return send_mail("Flexxzone Site", $title, $content
            , $GLOBALS['CONFIG']['emails']['admin']);
}

function send_mail($senderName, $subject, $body, $recipient) {
    $email = new PHPMailer();
    //$email->From = 'info@theflexxzone.com';
    $email->FromName = $senderName;
    $email->Subject = $subject;
    $email->Body = $body;
    $email->AddAddress($recipient);
    $email->IsHTML(true);
    $email->Send();
}

function redirect_to($url) {
    header("Location: $url");
    exit();
}

function validate_nigerian_number($number) {
    $prefixes = [];
    $countryCode = "234";
}

function validate_phone_number($number) {
    return preg_match('/^\+?[\d+]+$/', $number);
}

function fb_share_url($params, $smarty) {
    $shareLink = $params['share_url'];
    $redirectUrl = $params['redirect_url'];
    $url = "https://www.facebook.com/dialog/share?"
            . "app_id={$GLOBALS['CONFIG']['facebook']['app_id']}"
            . "&display=popup"
            . "&href=" . urlencode($shareLink)
            . "&redirect_uri=" . urlencode($redirectUrl);
    return $url;
}

function fb_share_url_2($params) {
    $shareLink = $params['share_url'];
    $redirectUrl = $params['redirect_url'];
    $title = $params['title'];
    $image = $params['image'];
    $description = $params['description'];
    $message = $params['message'];
    $url = "http://www.facebook.com/dialog/feed?"
            . "app_id={$GLOBALS['CONFIG']['facebook']['app_id']}"
            . "&link=" . urlencode($shareLink)
            . "&picture=" . urlencode($image)
            . "&caption=" . urlencode($title)
            . "&description=" . urlencode($description)
            . "&message=" . urlencode($message)
            . "&redirect_uri=" . urlencode($redirectUrl);
    return $url;
}

function add_flash($message, $type = FLASH_INFO) {
    $flash = get_flash();
    $flash[$type] = $message;
    $_SESSION['flash'] = $flash;
}

function get_flash() {
    $flash = !empty($_SESSION['flash']) ? $_SESSION['flash'] : [];
    clear_flash();
    return $flash;
}

function clear_flash() {
    $_SESSION['flash'] = [];
}

function twitter_share_url($params, $smarty) {
    $tweetMessage = urlencode($params['message']);
    $url = "https://twitter.com/intent/tweet"
            . "?text=$tweetMessage";
    return $url;
}

/**
 * Courtesy of Stackoverflow. No time!
 * 
 * @param type $updir
 * @param type $ext
 * @param type $imgName
 * @param int $width width of the thumbnail
 * @param int $height height of the thumbnail
 * 
 * @author alex
 */
function makeThumbnails($updir, $ext, $imgName, $width = 0, $height = 0) {
    $thumbnail_width = $width ? : 134;
    $thumbnail_height = $height ? : 189;
    $thumb_beforeword = "thumb";
    $arr_image_details = getimagesize("$updir" . $imgName . "$ext"); // pass id to thumb name
    $original_width = $arr_image_details[0];
    $original_height = $arr_image_details[1];
    if ($original_width > $original_height) {
        $new_width = $thumbnail_width;
        $new_height = intval($original_height * $new_width / $original_width);
    } else {
        $new_height = $thumbnail_height;
        $new_width = intval($original_width * $new_height / $original_height);
    }
    $dest_x = intval(($thumbnail_width - $new_width) / 2);
    $dest_y = intval(($thumbnail_height - $new_height) / 2);
    if ($arr_image_details[2] == 1) {
        $imgt = "ImageGIF";
        $imgcreatefrom = "ImageCreateFromGIF";
    }
    if ($arr_image_details[2] == 2) {
        $imgt = "ImageJPEG";
        $imgcreatefrom = "ImageCreateFromJPEG";
    }
    if ($arr_image_details[2] == 3) {
        $imgt = "ImagePNG";
        $imgcreatefrom = "ImageCreateFromPNG";
    }
    if ($imgt) {
        $old_image = $imgcreatefrom("$updir" . $imgName . "$ext");
        $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
        imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
        $imgt($new_image, "$updir" . $imgName . '_' . "$thumb_beforeword" . "$ext");
    }
}

function dd($var, $option = 1) {
    echo ('<pre>' . var_export($var, true) . '</pre>');
    if ($option) {
        exit;
    }
}

function clean($data) {
    if (is_array($data)) {
        foreach ($data as $key => $value) {
            $data[$key] = filter_var($value, FILTER_SANITIZE_STRING);
        }
        return $data;
    } else {
        return filter_var($data, FILTER_SANITIZE_STRING);
    }
}

function paginate($params){
    $smarty = new Smarty();
    $smarty->setTemplateDir(BASE_PATH . "templates");
    $smarty->assign("pagination", $params["object"]);
    return $smarty->fetch("_pagination.tpl");
}

function js_redirect_to($url) {
    $html = "<html>"
            . "<head>"
            . "<script>window.location = '$url';</script>"
            . "</head>"
            . "<body></body>"
            . "</html>";
    echo $html;
    exit();
}

function is_stage_2(){
    $now = time();
    $closureDate = $GLOBALS['CONFIG']['contest']['stage_2']['submission_closure_date'];
    $openingDate = $GLOBALS['CONFIG']['contest']['stage_2']['opening_date'];
    if ($now < strtotime($closureDate) && $now > strtotime($openingDate)) {
        return true;
    }
    return false;      
}

function is_stage_3(){
    $now = time();
    $closureDate = $GLOBALS['CONFIG']['contest']['stage_3']['submission_closure_date'];
    $openingDate = $GLOBALS['CONFIG']['contest']['stage_3']['opening_date'];
    if ($now < strtotime($closureDate) && $now > strtotime($openingDate)) {
        return true;
    }
    return false;    
}