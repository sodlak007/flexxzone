<?php

/*
 * Controller for the register action
 */

require_once './config.php';
require_once './securimage/securimage.php';

//check for contest closure
if (submission_closed()) {
    add_flash($MESSAGES["stage_2_submission_closed"], FLASH_INFO); 
    js_redirect_to(BASE_URL . "closed.php");
}

$errors = [];
$errorMessage = "";

$smarty->assignByRef("errors", $errors);
$smarty->assign("pageTitle", "Stage 2 " . CONTEST_NAME . " Entry Upload");

$entryHelper = EntryManager::instance();

if (filter_has_var(INPUT_POST, "apply")) {
    $cleanedData = cleanData();
    $captcha = filter_input(INPUT_POST, "captcha_code", FILTER_SANITIZE_STRING);
    if (validate($cleanedData)) {
        $entry = $entryHelper->get_entry_by_flexx_account($cleanedData['flexx_account_number']);
        $cleanedData['entry_id'] = $entry['id'];
        $securimage = new Securimage();
        if (empty($captcha)) {
            $errorMessage = "Please fill in the captcha";
        } else if (!$securimage->check($captcha)) {
            $errorMessage = "Could not validate that you are human. Please fill the captcha again";
        } else {
            $applyId = apply($cleanedData);
            if ($applyId) {
                //redirect
                redirect_to(BASE_URL . "stage_2_apply_success.php?ref=$applyId");
            }
        }
    }
}

$smarty->assign("errorMessage", $errorMessage);

$smarty->display("apply_stage_2.tpl");

function submission_closed() {
    $closureDate = $GLOBALS['CONFIG']['contest']['stage_2']['submission_closure_date'];
    if (strtotime($closureDate) < time()) {
        return true;
    }
    return false;
}

function cleanData() {
    $data = [];
    $data['flexx_account_number'] = filter_input(INPUT_POST, "flexx_account_number", FILTER_SANITIZE_STRING);
    $data['entry_image'] = $_FILES['entry_image'];
    return $data;
}

function validate($data) {
    $entryHelper = EntryManager::instance();
    if (empty($data['flexx_account_number'])) {
        $GLOBALS['errors']["flexx_account_number"] = "Please type in a valid Flexx account";
    } else if (!$entryHelper->flexx_account_number_entry_exists($data['flexx_account_number'])) {
        $GLOBALS['errors']["flexx_account_number"] = "The Flexx Account "
                . "Number you entered is not registered for this contest";
    }else if(!check_flexx_account_qualified($data['flexx_account_number'])){
        $GLOBALS['errors']["flexx_account_number"] = "Your Flexx account"
                . " number is not one of the stage 1 entries that qualified for stage 2";
    }else if(!check_unique_entries($data['flexx_account_number'])){
        $GLOBALS['errors']["flexx_account_number"] = "You can not upload more than one entry";
    }
    if (empty($data['entry_image']) || empty($data['entry_image']['tmp_name'])) {
        $GLOBALS['errors']["entry_image"] = "Please upload an image file";
    } else if (!validate_entry_image($data['entry_image'])) {
        $GLOBALS['errors']["entry_image"] = !empty($data['entry_image']['error']) ? "An error "
                . "occured while trying to upload your image entry" :
                "Please upload a valid image file not greater that 2Mb";
    }
    return empty($GLOBALS['errors']);
}

function check_flexx_account_qualified($flexx_account){
    $entryHelper = EntryManager::instance();
    $entry = $entryHelper->get_entry_by_flexx_account($flexx_account);
    if(empty($entry)){
        return false;
    }
    return $entry['is_winner'] == 1;
}

function validate_entry_image($upload) {
    return preg_match('/image\/*/', $upload['type']);
}

function apply($data) {
    //try uploading first
    $uploadPath = upload_entry($data['entry_image'], base64_encode($data['flexx_account_number']));
    if (empty($uploadPath)) {
        $GLOBALS['errorMessage'] = "Sign up failure. Failed to upload your image entry.";
        return false;
    }
    $sql = "INSERT INTO stage_2_entries (stage_1_id, entry_path) "
            . "VALUES('%s', '%s')";
    /**
     * @var Mysqli Mysql connection instance
     */
    $con = $GLOBALS['conn'];
    $query = sprintf($sql, $data['entry_id'], $uploadPath);
    $con->query($query);
    if ($con->affected_rows <= 0) {
        //delete the uploaded file
        Logger::log(Logger::LEVEL_ERROR
                , "Failed to register stage 2 participant entry: Sql error: " . $con->error, "Apply Error");
        $GLOBALS["errorMessage"] = "We are sorry but your entry could not be saved.";
    } else {
        return $con->insert_id;
    }
    return false;
}

function upload_entry($entryUploadData, $fileName) {
    $nameSplitted = explode(".", $entryUploadData['name']);
    $extension = array_pop($nameSplitted);
    $uploadPath = "apply" . DIRECTORY_SEPARATOR . "uploads"
            . DIRECTORY_SEPARATOR . "stage_2" . DIRECTORY_SEPARATOR;
    $uploadDir = BASE_PATH . $uploadPath;
    $fullFileName = $fileName . ($extension ? ".$extension" : "");
    $moveSTatus = move_uploaded_file($entryUploadData['tmp_name'], $uploadDir . $fullFileName);
    if ($moveSTatus) {
        //generate thumbnail
        generate_thumbnail($fileName, $extension, $uploadDir);
        return str_replace(DIRECTORY_SEPARATOR, "/", $uploadPath) . $fullFileName;
    }
    return false;
}

function generate_thumbnail($name, $ext, $dir) {
    makeThumbnails($dir, "." . $ext, $name, 269, 150);
}

function check_unique_entries($flexx_account) {
    $entryHelper = EntryManager::instance();
    $entry = $entryHelper->get_entry_by_flexx_account($flexx_account);
    if(empty($entry)){
        return false;
    }
    $stage2Entry = $entryHelper->get_stage_2_entry($entry['id']);
    return empty($stage2Entry);
}