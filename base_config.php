<?php

/*
 * The base config required by all sub application's configs
 */
//register error handler
set_error_handler("error_handler");
date_default_timezone_set("Africa/Lagos");

//determine the environment and load the appropriate file
$environment = file_exists(__DIR__ . DIRECTORY_SEPARATOR . "live.txt") ? "live" : "local";

define("IS_STAGING", $_SERVER["HTTP_HOST"] == "www.theflexxzone.com");
define("ENVIRONMENT", $environment);

define("BASE_PATH", dirname(__FILE__) . DIRECTORY_SEPARATOR);
define("SRC_FOLDER", "src");
define("SRC_PATH", BASE_PATH . SRC_FOLDER . DIRECTORY_SEPARATOR);
define("LOG_PATH", BASE_PATH . "logs" . DIRECTORY_SEPARATOR);
define("SYSTEM_LOG", LOG_PATH . "system.log");

define("CONTEST_NAME", "FCMB #FlexxYourCreativity");

require_once BASE_PATH . "autoloaders.php";
require_once BASE_PATH . "functions.php";
if (!is_live()) {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}

require_once BASE_PATH . "config" . DIRECTORY_SEPARATOR . ENVIRONMENT
        . "_config" . ".php";
require_once BASE_PATH . "config" . DIRECTORY_SEPARATOR . "gen_config.php";
require_once BASE_PATH . "config" . DIRECTORY_SEPARATOR . "messages.php";

require_once BASE_PATH . 'libs/Smarty.class.php';
require BASE_PATH . './phpmailer/class.phpmailer.php';

//do config mergin
$CONFIG = array_replace_recursive($GENCONFIG, $CONFIG);

session_start();

$smarty = new Smarty();

//now populate smarty with the necessary variables
$smarty->assign("MAX_UPLOAD_SIZE", 5e+6);
$smarty->assign("CONFIG", $CONFIG);
$smarty->assign("CONTEST_NAME", CONTEST_NAME);

//register plugins
$smarty->registerPlugin("function", "paginate", "paginate");

//$smarty->debugging = true;

$conn = new mysqli($CONFIG['db']['host'], $CONFIG['db']['user']
        , $CONFIG['db']['password'], $CONFIG['db']['dbName']);

if ($conn->connect_errno) {
    file_put_contents("logs.txt", "Failed to connect to MySQL: "
            . mysqli_connect_error());
    mail_error("Db Connection Failure", "Mysql error message: "
            . print_r(mysqli_error($conn), true));
}

$smarty->assign("flash", get_flash());

function error_handler($errno, $errstr, $errfile, $errline) {
    if (!(error_reporting() & $errno)) {
        // This error code is not included in error_reporting
        return;
    }

    $message = "$errstr on line $errline in $errfile";

    switch ($errno) {
        case E_USER_ERROR:
            if (!is_live()) {
                echo "<b>My ERROR</b> [$errno] $errstr<br />\n";
                echo "  Fatal error on line $errline in file $errfile";
                echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
                echo "Aborting...<br />\n";
                exit(1);
            }
            Logger::log(Logger::LEVEL_ERROR, $message);
            break;

        case E_USER_WARNING:
            if (!is_live()) {
                echo "<b>My WARNING</b> [$errno] $errstr<br />\n";
            }
            Logger::log(Logger::LEVEL_WARNING, $message);
            break;

        case E_USER_NOTICE:
            if (!is_live()) {
                echo "<b>My NOTICE</b> [$errno] $errstr<br />\n";
            }
            Logger::log(Logger::LEVEL_WARNING, $message);
            break;

        default:
            if (!is_live()) {
                echo "Unknown error type: [$errno] $errstr<br />\n";
            }
            Logger::log(Logger::LEVEL_INFO, $message);
            break;
    }

    /* Don't execute PHP internal error handler */
    return true;
}

function get_script_name() {
    $requestUrl = $_SERVER["REQUEST_URI"];
    $requestUrlSplitted = explode("/", $requestUrl);
    $scriptName = array_pop($requestUrlSplitted);
    if (strpos($scriptName, "?")) {
        $scriptName = explode("?", $scriptName)[0];
    }
    return $scriptName;
}
