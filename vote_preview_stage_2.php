<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once './config.php';

//default voting interval is 24 hours
define("VOTING_INTERVAL", 60 * 60 * 24);

$errorMessage = "";
$entryId = filter_input(INPUT_GET, "entry", FILTER_VALIDATE_INT);
$voteId = filter_input(INPUT_GET, "vote", FILTER_SANITIZE_NUMBER_INT);

$smarty->assign("voteId", $voteId);
$entryMgr = EntryManager::instance();

if (empty($entryId)) {
    add_flash("Select an entry to vote on", FLASH_ERROR);
    js_redirect_to(BASE_URL . "vote_stage_2.php");
}
if (!$entryMgr->stage_2_entry_exists($entryId)) {
    add_flash("The entry you are looking for does not exist", FLASH_ERROR);
    js_redirect_to(BASE_URL . "vote_stage_2.php");
}

if(voting_closed()){
    $errorMessage = $MESSAGES['stage_2_voting_closed'];
}

if (filter_has_var(INPUT_POST, "vote") && !voting_closed()) {
    $data = cleanData();
    $data['entry_id'] = $entryId;
    if (validate($data)) {
        $voteStatus = vote($data);
        $smarty->assign("voteStatus", $voteStatus);
        $smarty->assign("voteId", $voteStatus);
    }
}

$entry = $entryMgr->get_stage_2_entry($entryId);
$name = ucwords($entry["first_name"]) . " " . ucwords($entry["last_name"]);
$smarty->assign("pageTitle", "$name's " . CONTEST_NAME . " Stage 2 Contest Entry");
$smarty->assign("ogTitle", CONTEST_NAME . " Contest Stage 2");
$smarty->assign("ogDesc", "I just voted for " . ucwords($entry['first_name']) .
        " " . ucwords($entry['last_name']) . " in the second stage of " . CONTEST_NAME . " contest.");
$smarty->assign("ogImage", BASE_URL . $entry['thumbnail']);

//fetch the entry
$smarty->assign("entry", $entry);
$smarty->assign("errorMessage", $errorMessage);
$smarty->assign("shareMessage", get_share_message($entry));
$smarty->assign("is_voting_closed", voting_closed());

$smarty->display("vote-preview_stage_2.tpl");

function voting_closed(){
    $votingClosedDate = $GLOBALS['CONFIG']['contest']['stage_2']['vote_closure_date'];
    if(strtotime($votingClosedDate) < time()){
        return true;
    }
    return false;
}

function validate($data) {
    if (empty($data["voter_email"])) {
        $GLOBALS['errorMessage'] = "Please enter a valid email address";
    } else if (!check_same_day_voting($data["voter_email"])) {
        $GLOBALS['errorMessage'] = "You can only vote once everyday";
    }
    return empty($GLOBALS['errorMessage']);
}

function cleanData() {
    $cleanedData = [];
    $cleanedData['voter_email'] = filter_input(INPUT_POST, "voter_email", FILTER_VALIDATE_EMAIL);
    return $cleanedData;
}

function check_last_vote($email) {
    $latestVote = get_latest_vote($email);
    if (empty($latestVote)) {
        return true;
    }
    //check against allowed time interval
    //convert the time returned by mysql to php time
    $lastVoteTime = strtotime($latestVote['created_at']);
    $currentTime = time();
    $timeDiff = $currentTime - $lastVoteTime;
    return $timeDiff > VOTING_INTERVAL;
}

function check_same_day_voting($email) {
    $latestVote = get_latest_vote($email);
    if (empty($latestVote)) {
        return true;
    }
    //check against allowed time interval
    //convert the time returned by mysql to php time
    $lastVoteTime = strtotime($latestVote['created_at']);
    return date("Y-m-d") !== date("Y-m-d", $lastVoteTime);
}

function get_latest_vote($email) {
    $query = "SELECT * FROM stage_2_votes WHERE voter_email = '$email' ORDER BY created_at DESC LIMIT 1";
    $con = $GLOBALS['conn'];
    $results = $con->query($query);
    if (empty($results) || $results->num_rows == 0) {
        return false;
    }
    $row = $results->fetch_assoc();
    return $row;
}

function check_unique_email($email) {
    $con = $GLOBALS['conn'];
    $query = "SELECT * FROM votes where voter_email = '$email'";
    $results = $con->query($query);
    if (empty($results) || $results->num_rows == 0) {
        return true;
    }
    return false;
}

function vote($data) {
    $sql = "INSERT INTO stage_2_votes (stage_1_id, voter_email) VALUES('%d', '%s')";
    /**
     * @var Mysqli Mysql connection instance
     */
    $con = $GLOBALS['conn'];
    $query = sprintf($sql, $data['entry_id'], $data['voter_email']);
    $con->query($query);
    if ($con->affected_rows <= 0) {
        //delete the uploaded file
        mail_error("Voting Ersror", "Failed to register vote: Sql error: " . $con->error);
        $GLOBALS["errorMessage"] = "We are sorry but vote could not be recorded.";
    } else {
        return $con->insert_id;
    }
    return false;
}

function get_share_message($entry) {
    $name = ucwords($entry["first_name"]) . " " . ucwords($entry["last_name"]);
    $message = "I just voted for $name in the second stage of #FlexxYourCreativity Contest at"
            . " " . BASE_URL . "vote_preview_stage_2.php?entry={$entry['stage_1_id']}";
    return $message;
}