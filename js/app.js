$(document).foundation()

var media = $('#id_media, #id_media1, #id_media2, #id_media3');
  if (media.length) {
      var mediaDefaultValue = $('.file span.value').text();
      var mediaCharLimit = 20;

      $('.file .bt-value').click(function(){
          media.click();
      });

      media.on('change', function() {
          var value = this.value.replace("C:\\fakepath\\", "");
          var newValue;
          var valueExt;
          var charLimit;

          if (value) {
              newValue = value;
              valueExt = value.split('.').reverse()[0];
              if (newValue.length > mediaCharLimit) {
                  charLimit = mediaCharLimit - valueExt.length;

                  // truncate chars.
                  newValue = $.trim(value).substring(0, charLimit) + '…';

                  // if file name has extension, add it to newValue.
                  if (valueExt.length) {
                      newValue += valueExt;
                  }
              }
          }
          else {
        newValue = mediaDefaultValue;
      }
    $(this).parent().find('span.value').text(newValue);
  });
}

function setHeight() {
  windowHeight = $(window).innerHeight();
  $('.height-match').css('min-height', windowHeight);
};
setHeight();

$(window).resize(function() {
  setHeight();
});

$(document).ready(function(){
  $('.quest').click(function(){
    $(this).next().slideToggle();
  });

  $('.dark-header i').click(function(){
    $('.mobile-header').slideToggle();
  });

  $('.mobile-dropdown').click(function(e){
    e.preventDefault();
    $(this).next('.stages').slideToggle();
  });
})

