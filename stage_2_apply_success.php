<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once './config.php';

$entryId = filter_input(INPUT_GET, "ref", FILTER_SANITIZE_NUMBER_INT);

if (empty($entryId)) {
    add_flash("Invalid entry", FLASH_ERROR);
    redirect_to(BASE_URL . "apply_for_stage_2.php");
}

$entryHelper = EntryManager::instance();

//check if entry exists
if (!$entryHelper->stage_2_entry_id_exists($entryId)) {
    add_flash("The entry does not exist", FLASH_ERROR);
    redirect_to(BASE_URL . "apply.php");
}

$shareMessage = "I made it into stage 2. Please vote for me.";

$smarty->assign("entry", $entry = $entryHelper->get_stage_2_entry_by_id($entryId));
$smarty->assign("pageTitle", "Successfully Entered into Second Stage of the " . CONTEST_NAME . " Contest");
$smarty->assign("ogTitle", CONTEST_NAME . " Contest");
$smarty->assign("ogDesc", $shareMessage);
$smarty->assign("shareMessage", $shareMessage);
$smarty->assign("ogImage", BASE_URL . $entry['thumbnail']);

$smarty->display("stage_2_apply-success.tpl");
