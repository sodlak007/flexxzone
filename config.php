<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once './base_config.php';

$baseDirectorySplitted = explode(DIRECTORY_SEPARATOR
        , __DIR__);
//define base url
define("PROJECT_FOLDER", array_pop($baseDirectorySplitted));

define("BASE_URL", (is_live() && !IS_STAGING ? "https://" : "http://") . $CONFIG["site"]["base_url"] . "/"
        . PROJECT_FOLDER . "/");
define("BASE_URL_SSL", "https://" . $CONFIG["site"]["base_url"] . "/"
        . PROJECT_FOLDER . "/");

//now populate smarty with the necessary variables
$smarty->assign('BASE_URL', BASE_URL);
$smarty->assign('BASE_URL_SSL', BASE_URL_SSL);

$smarty->assign("is_stage_2", is_stage_2());
$smarty->assign("is_stage_3", is_stage_3());

//register plugins
$smarty->registerPlugin("function", "fb_share_url", "fb_share_url");
$smarty->registerPlugin("function", "fb_share_url_2", "fb_share_url_2");
$smarty->registerPlugin("function", "twitter_share_url", "twitter_share_url");