<?php

require_once './config.php';

$entryId = filter_input(INPUT_GET, "entry", FILTER_SANITIZE_NUMBER_INT);

$redirectUrl = $_SERVER["HTTP_REFERER"] ? 
        : BASE_URL . "entries.php";

if(empty($entryId)){
    add_flash("Entry to be promoted not specified", FLASH_ERROR);
    redirect_to($redirectUrl);
}

$entryHelper = EntryManager::instance();

$entry = $entryHelper->get_entry($entryId);

if(empty($entry)){
    add_flash("The entry could not be found", FLASH_ERROR);
    redirect_to($redirectUrl);
}

//promote to stage 2
$status = $entryHelper->promoteEntryToStage2($entryId);

if($status){
    add_flash("Entry has been promoted to stage 2. There are currently" 
            . $entryHelper->getStage1WinnersCount() .
            " winners in stage 2", FLASH_SUCCESS);
    js_redirect_to($redirectUrl);
}else{
    $errors = !empty($entryHelper->getErrors()) ? $entryHelper->getErrors() 
            : ["Failed to promote entry to stage 2"]; 
    foreach($errors as $error){
        add_flash($error, FLASH_ERROR);
    }
    js_redirect_to($redirectUrl);
}