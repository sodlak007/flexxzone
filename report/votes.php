<?php

require_once './config.php';

$redirectUrl = $_SERVER["HTTP_REFERER"] ? 
        : BASE_URL . "entries.php";

$entryId = filter_input(INPUT_GET, "entry", FILTER_SANITIZE_NUMBER_INT);

if(empty($entryId)){
    add_flash("No entry specified", FLASH_ERROR);
    redirect_to($redirectUrl);
}

$entryHelper = EntryManager::instance();

$entry = $entryHelper->get_entry($entryId);
if(empty($entry)){
    add_flash("The entry could not be found", FLASH_ERROR);
    redirect_to($redirectUrl);
}

$page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT) ? : 1;

//$ipp = filter_input(INPUT_GET, "ipp", FILTER_SANITIZE_NUMBER_INT) ? : 12;
$search = filter_input(INPUT_GET, "search", FILTER_SANITIZE_STRING) ? : "";
$entriesHelper = EntryManager::instance();
$totalItemsCount = $entriesHelper->get_vote_count($entryId);
$pagination = new Pagination(BASE_URL . "votes.php", $totalItemsCount, $page);

$smarty->assign("votes", $entriesHelper->get_votes($entryId, ($page - 1) * $pagination->getIpp()
                , $pagination->getIpp(), $search));
$smarty->assign("pageTitle", "{$entry['first_name']} {$entry['last_name']}'s Votes");
$smarty->assign("pagination", $pagination);
$smarty->assign("entry", $entry);

$smarty->display("votes.tpl");
