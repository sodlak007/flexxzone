<?php

require_once './config.php';

$entryId = filter_input(INPUT_GET, "entry", FILTER_SANITIZE_NUMBER_INT);

$redirectUrl = $_SERVER["HTTP_REFERER"] ? 
        : BASE_URL . "entries.php";

if(empty($entryId)){
    add_flash("Entry to be demoted not specified", FLASH_ERROR);
    redirect_to($redirectUrl);
}

$entryHelper = EntryManager::instance();

$entry = $entryHelper->get_stage_2_entry_by_id($entryId);

if(empty($entry)){
    add_flash("The entry could not be found", FLASH_ERROR);
    redirect_to($redirectUrl);
}

//promote to stage 2
$status = $entryHelper->removeToStage3Winner($entryId);

if($status){
    add_flash("Entry has been removed from stage 3. " 
            . $entryHelper->getStage2WinnersCount() .
            "entries promoted to stage 3", FLASH_SUCCESS);
    js_redirect_to($redirectUrl);
}else{
    $errors = !empty($entryHelper->getErrors()) ? $entryHelper->getErrors() 
            : ["Failed to remove entry from stage 3"]; 
    foreach($errors as $error){
        add_flash($error, FLASH_ERROR);
    }
    js_redirect_to($redirectUrl);
}