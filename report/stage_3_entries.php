<?php

require_once './config.php';

$page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT) ? : 1;

$ipp = filter_input(INPUT_GET, "ipp", FILTER_SANITIZE_NUMBER_INT) ? : 20;
$search = filter_input(INPUT_GET, "search", FILTER_SANITIZE_STRING) ? : "";
$sort = filter_input(INPUT_GET, "sort", FILTER_SANITIZE_NUMBER_INT) ? : 4;
$entryHelper = EntryManager::instance();
$totalItemsCount = $entryHelper->getStage2WinnersCount();
$pagination = new Pagination(BASE_URL . "stage_3_entries.php", $totalItemsCount, $page, $ipp);
$pagination->setShowIpp(true);

$smarty->assign("entries", $entryHelper->get_stage_3_winners(($page - 1) * $pagination->getIpp()
                , $ipp));
$smarty->assign("pageTitle", "Stage 2 Contest Winners & Entries");
$smarty->assign("pagination", $pagination);
$smarty->assign('sort', $sort);

$smarty->display("stage_3_entries.tpl");
