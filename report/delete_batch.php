<?php

require_once './config.php';

$entries = filter_input(INPUT_POST, "entries", FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);

$redirectUrl = $_SERVER["HTTP_REFERER"] ? : BASE_URL . "entries.php";

if (empty($entries)) {
    add_flash("Entries to be deleted not specified", FLASH_ERROR);
    redirect_to($redirectUrl);
}

//loop over the entries and delete
$totalEntries = count($entries);
$successful = 0;
$failed = 0;

$entryHelper = EntryManager::instance();

foreach ($entries as $entryId) {
    $entryDeleteStatus = $entryHelper->delete_entry($entryId);
    $voteDeleteStatus = $entryHelper->delete_entry_votes($entryId);

    if ($entryDeleteStatus && $voteDeleteStatus) {
        $successful++;
    } else {
        $failed++;
    }
}

if ($failed && $successful) {
    add_flash("$successful deleted successfully, failed to delete $failed entries", FLASH_INFO);
} elseif (empty($successful)) {
    add_flash("Failed to delete entries", FLASH_ERROR);
}else if(empty($failed)){
    add_flash("Successfully deleted selected entries", FLASH_SUCCESS);
}

redirect_to($redirectUrl);
