<?php

require_once './config.php';

$entryId = filter_input(INPUT_GET, "entry", FILTER_SANITIZE_NUMBER_INT);

$redirectUrl = $_SERVER["HTTP_REFERER"] ? 
        : BASE_URL . "entries.php";

if(empty($entryId)){
    add_flash("Entry to be deleted not specified", FLASH_ERROR);
    redirect_to($redirectUrl);
}

$entryHelper = EntryManager::instance();

$entry = $entryHelper->get_stage_3_entry($entryId);

if(empty($entry)){
    add_flash("The stage 3 entry could not be found", FLASH_ERROR);
    redirect_to($redirectUrl);
}

//promote to stage 2
$status = $entryHelper->makeContestWinner($entryId);

if($status){
    add_flash("The entry has been made the contest winner ", FLASH_SUCCESS);
    js_redirect_to($redirectUrl);
}else{
    $errors = !empty($entryHelper->getErrors()) ? $entryHelper->getErrors() 
            : ["Failed to make entry winner"]; 
    foreach($errors as $error){
        add_flash($error, FLASH_ERROR);
    }
    js_redirect_to($redirectUrl);
}