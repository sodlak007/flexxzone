<?php

require_once './config.php';

$entriesHelper = EntryManager::instance();

$totalItemsCount = $entriesHelper->get_all_stage_2_vote_count();

if (empty($totalItemsCount)) {
    add_flash("No voter emails to download", FLASH_INFO);
    redirect_to(BASE_URL . "stage_2_entries.php");
}
$filename = "all_stage_2_voter_emails.csv";
send_headers();

//create file
$handle = fopen($filename, "w");
$iterations = ceil($totalItemsCount / 1000);

$counter = 0;

$headings = ["#", "Email"];
fputcsv($handle, $headings);

for ($i = 0; $i < $iterations; $i++) {
    $entries = $entriesHelper->get_all_stage_2_votes($i, 1000);
    foreach ($entries as $entry) {
        $data = [
            "#" => ++$counter,
            "email" => $entry['voter_email']
        ];
        fputcsv($handle, $data);
    }
}
fclose($handle);
send_file($filename);
//delete file
unlink($filename);
exit();

function send_headers() {
    $filename = $GLOBALS['filename'];
    header('Content-Length:' . filesize($filename));
    header("Content-Type:text/csv");
    header('Content-Disposition:attachment; filename="' . $filename . '"');
}

function send_file($filename){
    readfile($filename);
}