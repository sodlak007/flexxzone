<?php

require_once './config.php';

$adminId = filter_input(INPUT_GET, "admin", FILTER_SANITIZE_NUMBER_INT);

$redirectUrl = $_SERVER["HTTP_REFERER"] ? : BASE_URL . "admins.php";

if (!AdminAuthManager::instance()->isLoggedAdminSuper()) {
    add_flash("You don't have permission to access this page", FLASH_ERROR);
    redirect_to($redirectUrl);
}

if (empty($adminId)) {
    add_flash("No admin specified", FLASH_ERROR);
    redirect_to($redirectUrl);
}

$adminManager = AdminAuthManager::instance();
$admin = $adminManager->getAdmin($adminId);

if (empty($admin)) {
    add_flash("The admin does not exist", FLASH_ERROR);
    redirect_to($redirectUrl);
}

$smarty->assign('admin', $admin);

$errors = [];

if (filter_has_var(INPUT_POST, "reset_password")) {
    $data = clean_data();
    if (validate($data)) {
        $resetStatus = $adminManager->resetAdminPassword($adminId, $data['password']);
        $message = $resetStatus ? "Successfully reset admin password" : "Failed to reset admin password";
        add_flash($message, $resetStatus ? FLASH_SUCCESS : FLASH_ERROR);
        if ($resetStatus) {
            redirect_to($redirectUrl);
        }
    }
}

$smarty->assign("errors", $errors);

$smarty->display("reset_admin_password.tpl");

function validate($data) {
    $errors = [];
    if (empty($data['password'])) {
        $errors['password'] = "Please type in a password";
    } else if (!$GLOBALS['adminManager']->validatePassword($data['password'])) {
        $errors = array_merge($errors, $GLOBALS['adminManager']->getErrors());
    }
    if ($data['password'] != $data['password_confirm']) {
        $errors['password_confirm'] = "Type in the password again to confirm";
    }
    $GLOBALS['errors'] = $errors;
    return empty($errors);
}

function clean_data() {
    $data = [];
    $data['password'] = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
    $data['password_confirm'] = filter_input(INPUT_POST, "password_confirm", FILTER_SANITIZE_STRING);
    return $data;
}
