<?php

require_once './config.php';

$sortMap = [
    1 => "asc",
    2 => "desc",
    3 => "created_at asc",
    4 => "created_at desc",
];

$sortOptions = [
    1 => "Arrange By Lowest Votes",
    2 => "Arrange By Highest Votes",
    3 => "Arrange By Least Recent Entry",
    4 => "Arrange By Most Recent Entry",
];

$page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT) ? : 1;

$ipp = filter_input(INPUT_GET, "ipp", FILTER_SANITIZE_NUMBER_INT) ? : 20;
$search = filter_input(INPUT_GET, "search", FILTER_SANITIZE_STRING) ? : "";
$sort = filter_input(INPUT_GET, "sort", FILTER_SANITIZE_NUMBER_INT) ? : 4;
$entryHelper = EntryManager::instance();
$totalItemsCount = $entryHelper->get_entries_count();
$pagination = new Pagination(BASE_URL . "entries.php", $totalItemsCount, $page, $ipp);
$pagination->setShowIpp(true);

if ($sort == 1 || $sort == 2) {
    $smarty->assign("entries", $entryHelper->get_entries_by_count(($page - 1) * $pagination->getIpp()
                    , $ipp, $sortMap[$sort]));
} else {
    $sort = isset($sortMap[$sort]) ? $sort : 4;
    $smarty->assign("entries", $entryHelper->get_entries(($page - 1) * $pagination->getIpp()
                    , $ipp, $sortMap[$sort]));
}

$smarty->assign("pageTitle", "Contest Entries");
$smarty->assign("pagination", $pagination);
$smarty->assign("sortOptions", $sortOptions);
$smarty->assign('sort', $sort);

$smarty->display("entries.tpl");
