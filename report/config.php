<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once '../base_config.php';

$baseDirectorySplitted = explode(DIRECTORY_SEPARATOR
        , __DIR__);
//define base url
define("PROJECT_FOLDER", $baseDirectorySplitted[count($baseDirectorySplitted) - 1]);
define("PROJECT_PATH", join(DIRECTORY_SEPARATOR, array_slice($baseDirectorySplitted, -2)));

define("BASE_URL", (is_live() && !IS_STAGING ? "https://" : "http://") . $CONFIG["site"]["base_url"] . "/"
        . str_replace(DIRECTORY_SEPARATOR, "/", PROJECT_PATH) . "/");
define("BASE_URL_ORIG", str_replace(PROJECT_FOLDER . "/", "", BASE_URL));
define("CDN_URL", BASE_URL_ORIG);
define("BASE_URL_SSL", "https://" . $CONFIG["site"]["base_url"] . "/"
        . str_replace(DIRECTORY_SEPARATOR, "/", PROJECT_PATH) . "/");

//now populate smarty with the necessary variables
$smarty->assign('BASE_URL', BASE_URL);
$smarty->assign('BASE_URL_SSL', BASE_URL_SSL);
$smarty->assign('CDN_URL', CDN_URL);
$smarty->assign('is_logged_in', AdminAuthManager::instance()->isLoggedIn());
$smarty->assign('is_super_admin', AdminAuthManager::instance()->isLoggedAdminSuper());

$nonAuthPages = ["login.php"];

if(!AdminAuthManager::instance()->isLoggedIn() && !in_array(get_script_name(), $nonAuthPages)){
    redirect_to(BASE_URL . "login.php?force=1");
}