<?php

require_once './config.php';

$redirectUrl = $_SERVER["HTTP_REFERER"] ? : BASE_URL . "admins.php";

if (!AdminAuthManager::instance()->isLoggedAdminSuper()) {
    add_flash("You don't have permission to access this page", FLASH_ERROR);
    redirect_to($redirectUrl);
}

$adminId = filter_input(INPUT_GET, "admin", FILTER_SANITIZE_NUMBER_INT);

if (empty($adminId)) {
    add_flash("No admin specified", FLASH_ERROR);
    redirect_to($redirectUrl);
}

$adminManager = AdminAuthManager::instance();
$admin = $adminManager->getAdmin($adminId);

if (empty($admin)) {
    add_flash("The admin does not exist", FLASH_ERROR);
    redirect_to($redirectUrl);
}

$status = filter_input(INPUT_GET, "status", FILTER_SANITIZE_NUMBER_INT);

if ($status && $admin['is_active']) {
    add_flash("Admin is already active", FLASH_INFO);
    redirect_to($redirectUrl);
} elseif (!$status && !$admin['is_active']) {
    add_flash("Admin is already deactivated", FLASH_INFO);
    redirect_to($redirectUrl);
}

if ($status) {
    $statusChange = $adminManager->activateAdmin($adminId);
} else {
    $statusChange = $adminManager->deactivateAdmin($adminId);
}

$verb = $status ? "Activation" : "Deactivation";
if ($statusChange) {
    $message = "$verb of admin was successful";
} else {
    $message = "$verb of admin failed";
}

add_flash($message, $statusChange ? FLASH_SUCCESS : FLASH_ERROR);
redirect_to($redirectUrl);
