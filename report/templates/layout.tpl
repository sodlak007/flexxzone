<!doctype html>
<html class="no-js" lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <title>{$pageTitle|default:'Flexxzone Contest Admin'}</title>
        <link rel="stylesheet" href="{CDN_URL}css/foundation.min.css">
        <link rel="stylesheet" href="{CDN_URL}css/font-awesome.min.css">
        <link rel="stylesheet" href="{CDN_URL}css/app.css">

        {block 'head_css'}

        {/block}

        {block 'head_js'}

        {/block}
    </head>
    <body>
        <div class="dark-header">
            <div class="row">
                <div class="large-5 medium-5 small-7 columns">
                    <a href="{BASE_URL}">
                        <img src="{CDN_URL}img/logo.png" width="200" height="50">
                    </a>
                </div>

                <div class="large-7 medium-7 small-5 columns">
                    <ul>
                        <li><a href="#" class="dropdown">Entries</a>
                            <ul>
                                <li><a href="{$BASE_URL}entries.php">Stage 1</a></li>
                                <li><a href="{$BASE_URL}stage_2_entries.php">Stage 2</a></li>
                                <li><a href="{$BASE_URL}stage_3_entries.php">Stage 3</a></li>
                            </ul>
                        </li>
                        <!--<li><a href="{$BASE_URL}entries.php">Entries</a></li>-->
                        <li>
                            {if $is_super_admin}
                                <a href="{$BASE_URL}admins.php">Admins</a>
                            {/if}
                        </li>
                        <li>
                            <a href="{$BASE_URL}{if $is_logged_in}logout{else}login{/if}.php" class="rounded">
                                {if $is_logged_in}Logout{else}Log in{/if}
                            </a>
                        </li>
                    </ul>
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </div>
            </div>
        </div>

        <div class="mobile-header">
            <a href="#" class="mobile-dropdown">Entries</a>
            <div class="stages">
                <a href="{$BASE_URL}entries.php">Stage 1</a>
                <a href="{$BASE_URL}stage_2_entries.php">Stage 2</a>
                <a href="{$BASE_URL}stage_3_entries.php">Stage 3</a>
            </div>
            <!--<a href="{$BASE_URL}entries.php">Entries</a>-->
            {if $is_super_admin}
                <a href="{$BASE_URL}admins.php">Admins</a>
            {/if}
            <a href="{$BASE_URL}{if $is_logged_in}logout{else}login{/if}.php" class="rounded">
                {if $is_logged_in}Logout{else}Log in{/if}
            </a>
        </div>

        <div>
            {if !empty($flash.error)}
                {foreach from=$flash.error item="fl_error"}
                    <div class="vote-error">
                        {$fl_error}
                    </div>
                {/foreach}
            {/if}
            {if !empty($flash.info)}
                {foreach from=$flash.info item="fl_info"}
                    <div class="vote-warning">
                        {$fl_info}
                    </div>
                {/foreach}
            {/if}
            {if !empty($flash.success)}
                {foreach from=$flash.success item="fl_success"}
                    <div class="vote-success">
                        {$fl_success}
                    </div>
                {/foreach}
            {/if}
        </div>

        {block name="content"}

        {/block}

        <div class="footer">
            <div class="row">
                <div class="large-4 medium-12 small-12 columns">
                    <p>© 2016 FCMB. All Rights Reserved.</p>
                </div>
                <div class="large-4 medium-12 small-12 columns">
                    <div class="footer-links">
                        <a href="http://www.fcmb.com/sitemap/privacy-policy">Privacy Policy</a>
                        <a href="http://www.fcmb.com/">Visit FCMB</a>
                    </div>
                </div>
                <div class="large-4 medium-12 small-12 columns">  
                    <div class="social">
                        <ul>
                            <li><a href="https://www.facebook.com/fcmbmybank/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.twitter.com/myfcmb/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.instagram.com/myfcmb/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.youtube.com/user/fcmbplc" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <script src="{CDN_URL}js/vendor/jquery.js"></script>
        <script src="{CDN_URL}js/vendor/what-input.js"></script>
        <script src="{CDN_URL}js/vendor/foundation.js"></script>
        <script src="{CDN_URL}js/app.js"></script>

        {block 'bottom_js'}

        {/block}
    </body>
</html>
