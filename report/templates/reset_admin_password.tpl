{extends 'layout.tpl'}

{block 'head_css'}
    <style>
        .photoSignUp input[type='text']{
            padding: 10px 10px;
        }
    </style>
{/block}

{block 'content'}
    <div class="photoSignUp">
        <div class="row">
            <div class="large-6 large-centered medium-12 small-11 small-centered columns">
                {if !empty($errors.others)}
                    <div class="row vote-error">
                        <ul class="error">
                            {foreach from=$errors.others item="error"}
                                <li>{$error}</li>
                                {/foreach}
                        </ul>
                    </div>
                {/if}
                <form method="post" action="{$BASE_URL}reset_admin_password.php?admin={$admin.id}">
                    <div class="row" style="margin-bottom: 10px">
                        <center>
                            <small style='color:#f08a24'>* Password should be 10 or more characters with atleast 2 numbers</small>
                        </center>
                    </div>
                    <div class="row">
                        <label>
                            Password:
                            <input type='password' name="password" value="{$smarty.post.password|default:''}" />
                        </label>
                        {if !empty($errors.password)}
                            <span class='error'>{$errors.password}</span>
                        {/if}
                    </div>
                    <div class="row">
                        <label>
                            Confirm Password:
                            <input type='password' name="password_confirm" value="{$smarty.post.password_confirm|default:''}" />
                        </label>
                        {if !empty($errors.password_confirm)}
                            <span class='error'>{$errors.password_confirm}</span>
                        {/if}
                    </div>
                    <div class="row">
                        <input type='submit' name="reset_password" value="Reset Password" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{/block}