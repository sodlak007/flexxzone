{extends 'layout.tpl'}

{block 'content'}
    <div class="photoSignUp">
        <div class="row">
            <div class="large-6 large-centered medium-12 small-11 small-centered columns">
                <div class="row">
                    <form method="post" action="{$BASE_URL}login.php">
                        <div class="row">
                            <label>
                                Email:
                                <input type='text' name="email" value="{$smarty.post.email|default:''}" />
                            </label>
                        </div>
                        <div class="row">
                            <label>
                                Password:
                                <input type='password' name="password" value="{$smarty.post.password|default:''}" />
                            </label>
                        </div>
                        <div class="row">
                            <input type="submit" name="login" value="Log In" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>    
{/block}