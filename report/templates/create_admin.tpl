{extends 'layout.tpl'}

{block 'head_css'}
    <style>
        .photoSignUp input[type='text']{
            padding: 10px 10px;
        }
    </style>
{/block}

{block 'content'}
    <div class="photoSignUp">
        <div class="row">
            <div class="large-6 large-centered medium-12 small-11 small-centered columns">
                {if !empty($errors.others)}
                    <div class="row vote-error">
                        <ul class="error">
                            {foreach from=$errors.others item="error"}
                                <li>{$error}</li>
                                {/foreach}
                        </ul>
                    </div>
                {/if}
                <form method="post" action="{$BASE_URL}create_admin.php">
                    <div class="row" style="margin-bottom: 10px">
                        <center>
                            <small style='color:#f08a24'>* Password should be 10 or more characters with atleast 2 numbers</small>
                        </center>
                    </div>
                    <div class="row">
                        <label>
                            Email:
                            <input type='text' name="email" value="{$smarty.post.email|default:''}" />
                        </label>
                        {if !empty($errors.email)}
                            <span class='error'>{$errors.email}</span>
                        {/if}
                    </div>
                    <div class="row">
                        <label>
                            Password:
                            <input type='password' name="password" value="{$smarty.post.password|default:''}" />
                        </label>
                        {if !empty($errors.password)}
                            <span class='error'>{$errors.password}</span>
                        {/if}
                    </div>
                    <div class="row">
                        <label>
                            Confirm Password:
                            <input type='password' name="password_confirm" value="{$smarty.post.password_confirm|default:''}" />
                        </label>
                        {if !empty($errors.password_confirm)}
                            <span class='error'>{$errors.password_confirm}</span>
                        {/if}
                    </div>
                    <div class="row">
                        <div class="large-6 columns medium-6 small-12">
                            <label>
                                <input type='checkbox' name="is_active" value="1"
                                       {if !empty($smarty.post.is_active) || !isset($smarty.post.create_admin)}checked='checked'{/if} />
                                Activate:
                            </label>
                        </div>
                        <div class="large-6 columns medium-6 small-12">
                            <label>
                                <input type='checkbox' name="is_super" value="1" 
                                       {if !empty($smarty.post.is_super)}checked='checked'{/if} />
                                Make Super Admin:
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <input type='submit' name="edit_admin" value="Create Admin" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{/block}