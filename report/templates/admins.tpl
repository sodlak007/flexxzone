{extends 'layout.tpl'}

{block 'content'}
    <div class="photoSignUp">
        <div class="row">
            <div class="large-12 large-centered medium-12 small-11 small-centered columns">
                <div class="row">
                    <form method="post" action="" id="entriesForm">
                        <ul class="button-group">
                            <li>
                                <a href='{$BASE_URL}create_admin.php' class="button">Create Admin</a>
                            </li>
                            <!--<li><a href="#" class="button">Button 2</a></li>
                            <li><a href="#" class="button">Button 3</a></li>-->
                        </ul>
                        <table>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Email</th>
                                    <th>Date Created</th>
                                    <th>Options</th>
                                </tr>
                            </thead>

                            <tbody>
                                {foreach from=$admins item="admin" name="adminsLoop"}
                                    <tr>
                                        <td>
                                            {$smarty.foreach.adminsLoop.iteration + $pagination->getOffset()}
                                        </td>
                                        <td>{$admin.email}</td>
                                        <td>{$admin.created_at|date_format:'%B %d, %Y'}</td>
                                        <td>
                                            <a href="{$BASE_URL}admin_change_status.php?admin={$admin.id}&status={if $admin.is_active == 1}0{else}1{/if}">
                                                {if $admin.is_active == 1}
                                                    Deactivate
                                                {else}
                                                    Activate
                                                {/if}
                                            </a>&nbsp;
                                            <a href="{$BASE_URL}edit_admin.php?admin={$admin.id}">Edit</a>&nbsp;
                                            <a href="{$BASE_URL}reset_admin_password.php?admin={$admin.id}">Reset Password</a>
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </form>

                    {paginate object=$pagination}
                </div>
            </div>
        </div>
    </div>    
{/block}