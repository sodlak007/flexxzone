{extends 'layout.tpl'}

{block 'bottom_js'}
    <script>
        $('#delete-votes').on('click', function (e) {
            e.preventDefault();
            var href = $(this).attr("href");
            var conf = confirm("Are you sure you want to delete all this entry's votes?");
            if (conf) {
                window.location = href;
            }
        });
    </script>
{/block}

{block 'content'}    
    <div class="row">
        <div class="large-12 large-centered medium-12 small-11 small-centered columns">
            <div class="row">                
                <div class="small-12 columns">
                    <div class="big-image-ctn">
                        <img src="{$CDN_URL}{$entry.entry_path}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="small-12 columns">
                    <a href="{$BASE_URL}delete_votes.php?entry={$entry.id}"
                       class="button alert" id="delete-votes">
                        Delete All Votes
                    </a>
                    <table>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Email</th>
                                <th>Date</th>
                                <th>Option</th>
                            </tr>
                        </thead>

                        <tbody>
                            {foreach from=$votes item="vote"}
                                <tr>
                                    <td>{$smarty.foreach.entriesLoop.iteration + $pagination->getOffset()}</td>
                                    <td>{$vote.voter_email}</td>
                                    <td>{$vote.created_at|date_format:'%B %d, %Y'}</td>
                                    <td>
                                        <a href="#" class="delete-link">Delete</a>
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
{/block}