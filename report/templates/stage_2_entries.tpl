{extends 'layout.tpl'}

{block 'head_css'}
    <style>
        .button-group li{
            float: left;
        }
    </style>
{/block}

{block 'bottom_js'}
    {literal}
        <script>
            $(document).ready(function () {
                $('.delete-entry').on('click', function (e) {
                    e.preventDefault();
                    var href = $(this).attr("href");
                    var conf = confirm("Are you sure you want to delete this entry? " +
                            "This will also delete all votes for this entry");
                    if (conf) {
                        window.location = href;
                    }
                });
            });
        </script>
    {/literal}
{/block}

{block 'content'}
    <div class="photoSignUp">
        <div class="row">
            <div>
                <center>
                    <h3>Stage 2 Winner's Entries</h3>
                </center>
            </div>
            <div class="large-12 large-centered medium-12 small-11 small-centered columns">                
                <form method="post" action="{$BASE_URL}delete_batch.php" id="entriesForm">
                    <div class="row">
                        <div class="large-12 columns">
                            <div class='large-5 columns'>
                                <ul class="button-group">s
                                    <li>
                                        <a href="{$BASE_URL}download_all_stage_2_voter_emails.php" class="button">
                                            Download Voter Emails
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <table>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Number</th>
                                    <th>Email</th>
                                    <th>Flexx Account</th>
                                    <th>Entry</th>
                                    <th>Votes</th>
                                    <th>Date</th>
                                    <th>Options</th>
                                </tr>
                            </thead>

                            <tbody>
                                {foreach from=$entries item="entry" name="entriesLoop"}
                                    <tr>
                                        <td>{$smarty.foreach.entriesLoop.iteration + $pagination->getOffset()}</td>
                                        <td>{$entry.first_name} {$entry.last_name}</td>
                                        <td>{$entry.phone_number}</td>
                                        <td>{$entry.email}</td>
                                        <td>{$entry.flexx_account_number}</td>
                                        <td>
                                            {if $entry.thumbnail}
                                                <a href="#">
                                                    <img src="{CDN_URL}{$entry.thumbnail}" />
                                                </a>
                                            {else}
                                                No upload yet
                                            {/if}
                                        </td>
                                        <td>{$entry.vote_count}</td>
                                        <td>{$entry.created_at|date_format:'%B %d, %Y'}</td>
                                        <td>
                                            {if $entry.thumbnail}
                                                <a href="{$BASE_URL}promote_to_stage_3.php?entry={$entry.stage_1_id}">
                                                    Select as Winner
                                                </a>&nbsp;|
                                            {/if}
                                            <a href="{$BASE_URL}delete_stage_2_entry.php?entry={$entry.stage_1_id}"
                                               class="delete-entry delete-link" data-entry-id='{$entry.id}'>
                                                Delete Entry
                                            </a>&nbsp;
                                            {*<a href="{$BASE_URL}votes.php?entry={$entry.id}">Votes</a>*}
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>

                        {paginate object=$pagination}
                    </div>
                </form>
            </div>
        </div>
    </div>
{/block}