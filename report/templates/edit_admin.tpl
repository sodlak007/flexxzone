{extends 'layout.tpl'}

{block 'head_css'}
    <style>
        .photoSignUp input[type='text']{
            padding: 10px 10px;
        }
    </style>
{/block}

{block 'content'}
    <div class="photoSignUp">
        <div class="row">
            <div class="large-6 large-centered medium-12 small-11 small-centered columns">
                {if !empty($errors.others)}
                    <div class="row vote-error">
                        <ul class="error">
                            {foreach from=$errors.others item="error"}
                                <li>{$error}</li>
                                {/foreach}
                        </ul>
                    </div>
                {/if}
                {if isset($smarty.post.edit_admin)}
                    {assign var="adminValue" value=$smarty.post}
                {else}                    
                    {assign var="adminValue" value=$admin}
                {/if}
                <form method="post" action="{$BASE_URL}edit_admin.php?admin={$admin.id}">
                    <div class="row">
                        <label>
                            Email:
                            <input type='text' name="email" value="{$adminValue.email|default:''}" />
                        </label>
                        {if !empty($errors.email)}
                            <span class='error'>{$errors.email}</span>
                        {/if}
                    </div>
                    <div class="row">
                        <div class="large-6 columns medium-6 small-12">
                            <label>
                                <input type='checkbox' name="is_active" value="1"
                                       {if !empty($adminValue.is_active) || !isset($adminValue.create_admin)}checked='checked'{/if} />
                                Activate:
                            </label>
                        </div>
                        <div class="large-6 columns medium-6 small-12">
                            <label>
                                <input type='checkbox' name="is_super" value="1" 
                                       {if !empty($adminValue.is_super)}checked='checked'{/if} />
                                Make Super Admin:
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <input type='submit' name="edit_admin" value="Edit Admin" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{/block}