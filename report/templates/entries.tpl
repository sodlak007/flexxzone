{extends 'layout.tpl'}

{block 'head_css'}
    <style>
        .button-group li{
            float: left;
        }
    </style>
{/block}

{block 'bottom_js'}
    {literal}
        <script>
            $(document).ready(function () {
                $('.delete-entry').on('click', function (e) {
                    e.preventDefault();
                    var href = $(this).attr("href");
                    var conf = confirm("Are you sure you want to delete this entry? " +
                            "This will also delete all votes for this entry");
                    if (conf) {
                        window.location = href;
                    }
                });

                $('#selectAllCheck').on('click', function () {
                    if ($('#selectAllCheck').is(":checked")) {
                        $('.entry-checkbox').prop("checked", true);
                    } else {
                        $('.entry-checkbox').prop("checked", false);
                    }
                });

                $('#entriesForm').on('submit', function () {
                    if ($('.entry-checkbox:checked').length < 1) {
                        alert("Please select one or more entries to perform batch action on");
                        return false;
                    }
                    return true;
                });

                //for sort select
                $('#sortSelect').on('change', function () {
                    var sort = $(this).val();
                    var currentUrl = window.location.href;
                    var newUrl = "";
                    if (currentUrl.indexOf("?") < 0) {
                        newUrl = currentUrl + "?sort=" + sort;
                    } else {
                        var currentUrlSplitted = currentUrl.split("?");
                        var querySplitted = currentUrlSplitted[1].split("&");
                        var queryObj = {};
                        for (var i = 0; i < querySplitted.length; i++) {
                            var queryParts = querySplitted[i].split("=");
                            var name = queryParts[0];
                            var value = queryParts[1];
                            queryObj[name] = value;
                        }
                        queryObj.sort = sort;
                        newUrl = currentUrlSplitted[0] + "?";
                        var newUrlParts = [];
                        for (var name in queryObj) {
                            newUrlParts.push(name + "=" + queryObj[name]);
                        }
                        newUrl += newUrlParts.join("&");
                    }
                    window.location = newUrl;
                });
            });
        </script>
    {/literal}
{/block}

{block 'content'}
    <div class="photoSignUp">
        <div class="row">
            <center>
                <h3>Stage 1 Entries</h3>
            </center>
            <div class="large-12 large-centered medium-12 small-11 small-centered columns">                
                <form method="post" action="{$BASE_URL}delete_batch.php" id="entriesForm">
                    <div class="row">
                        <div class="large-12 columns">
                            <div class='large-5 columns'>
                                <ul class="button-group">
                                    <li>
                                        <button type="submit" value="delete" class="button">Delete</button>
                                    </li>
                                    <li><a href="{$BASE_URL}download_all.php" class="button">Download All</a></li>
                                    <li><a href="{$BASE_URL}download_all_voter_emails.php" class="button">Download Voter Emails</a></li>
                                </ul>
                            </div>
                            <div style="float: right" class="large-5 columns">
                                <select id="sortSelect">
                                    {foreach from=$sortOptions item="text" key="key"}
                                        <option value="{$key}" {if $sort == $key}selected='selected'{/if}>
                                            {$text}
                                        </option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <table>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><input type='checkbox' name='' id="selectAllCheck" value="1"/></th>
                                    <th>Name</th>
                                    <th>Number</th>
                                    <th>Email</th>
                                    <th>Flexx Account</th>
                                    <th>Entry</th>
                                    <th>Votes</th>
                                    <th>Date</th>
                                    <th>Options</th>
                                </tr>
                            </thead>

                            <tbody>
                                {foreach from=$entries item="entry" name="entriesLoop"}
                                    <tr>
                                        <td>{$smarty.foreach.entriesLoop.iteration + $pagination->getOffset()}</td>
                                        <td><input type='checkbox' name='entries[]' class="entry-checkbox" value="{$entry.id}" /></td>
                                        <td>{$entry.first_name} {$entry.last_name}</td>
                                        <td>{$entry.phone_number}</td>
                                        <td>{$entry.email}</td>
                                        <td>{$entry.flexx_account_number}</td>
                                        <td>
                                            <a href="#">
                                                <img src="{CDN_URL}{$entry.thumbnail}" />
                                            </a>
                                        </td>
                                        <td>{$entry.vote_count}</td>
                                        <td>{$entry.created_at|date_format:'%B %d, %Y'}</td>
                                        <td>
                                            <a href="{$BASE_URL}promote_to_stage_2.php?entry={$entry.id}">
                                                Select as Winner
                                            </a>
                                            <a href="{$BASE_URL}delete_entry.php?entry={$entry.id}"
                                               class="delete-entry delete-link" data-entry-id='{$entry.id}'>
                                                Delete
                                            </a>&nbsp;
                                            <a href="{$BASE_URL}votes.php?entry={$entry.id}">Votes</a>
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>

                        {paginate object=$pagination}
                    </div>
                </form>
            </div>
        </div>
    </div>
{/block}