<?php

require_once './config.php';

$entryId = filter_input(INPUT_GET, "entry", FILTER_SANITIZE_NUMBER_INT);

$redirectUrl = $_SERVER["HTTP_REFERER"] ? 
        : BASE_URL . "votes.php?entry=$entryId";

if(empty($entryId)){
    add_flash("Entry to be deleted not specified", FLASH_ERROR);
    redirect_to($redirectUrl);
}

$entryHelper = EntryManager::instance();

$entry = $entryHelper->get_entry($entryId);

if(empty($entry)){
    add_flash("The entry could not be found", FLASH_ERROR);
    redirect_to($redirectUrl);
}

//first delete all entry votes
$voteDeleteStatus = $entryHelper->delete_entry_votes($entryId);

if($voteDeleteStatus){
    add_flash("Successfully deleted entry votes", FLASH_SUCCESS);
}else{
    add_flash("Failed to delete entry votes", FLASH_ERROR);
}

redirect_to($redirectUrl);