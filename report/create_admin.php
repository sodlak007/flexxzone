<?php

require_once './config.php';

$redirectUrl = $_SERVER["HTTP_REFERER"] ? : BASE_URL . "admins.php";

if (!AdminAuthManager::instance()->isLoggedAdminSuper()) {
    add_flash("You don't have permission to access this page", FLASH_ERROR);
    redirect_to($redirectUrl);
}

if(filter_has_var(INPUT_POST, "create_admin")){
    $adminManager = AdminAuthManager::instance();
    $data = clean_data();
    if($adminManager->validateData($data)){
        $createStatus = $adminManager->createAdmin($data);
        if($createStatus){
            add_flash("Successfully created admin", FLASH_SUCCESS);
            redirect_to(BASE_URL . "admins.php");
        }else{
            add_flash("Failed to create admin", FLASH_ERROR);
        }
    }else{
        dd($adminManager->getErrors());
        $smarty->assign("errors", $adminManager->getErrors());
    }
}

$smarty->display("create_admin.tpl");

function clean_data(){
    $data = [];
    $data['email'] = filter_input(INPUT_POST, "email", FILTER_SANITIZE_STRING);
    $data['password'] = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
    $data['password_confirm'] = filter_input(INPUT_POST, "password_confirm", FILTER_SANITIZE_STRING);
    $data['is_active'] = filter_input(INPUT_POST, "is_active", FILTER_SANITIZE_NUMBER_INT);
    $data['is_super'] = filter_input(INPUT_POST, "is_super", FILTER_SANITIZE_NUMBER_INT);
    return $data;
}