<?php

require_once './config.php';

$adminManager = AdminAuthManager::instance();

$redirectUrl = $_SERVER["HTTP_REFERER"] ? : BASE_URL . "admins.php";

if (!$adminManager->isLoggedAdminSuper()) {
    add_flash("You don't have permission to access this page", FLASH_ERROR);
    redirect_to(BASE_URL);
}

$page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT) ? : 1;

$ipp = filter_input(INPUT_GET, "ipp", FILTER_SANITIZE_NUMBER_INT) ? : 12;
$totalItemsCount = $adminManager->getAdminsCount();
$pagination = new Pagination(BASE_URL . "admins.php", $totalItemsCount, $page);

$smarty->assign("admins", $adminManager->getAdmins(($page - 1) * $pagination->getIpp()
                , $pagination->getIpp()));
$smarty->assign("pageTitle", "Admins");
$smarty->assign("pagination", $pagination);

$smarty->display("admins.tpl");
