<?php

require_once './config.php';

$adminId = filter_input(INPUT_GET, "admin", FILTER_SANITIZE_NUMBER_INT);

$redirectUrl = BASE_URL . "admins.php";

if (!AdminAuthManager::instance()->isLoggedAdminSuper()) {
    add_flash("You don't have permission to access this page", FLASH_ERROR);
    redirect_to($_SERVER["HTTP_REFERER"] ? : $redirectUrl);
}

if (empty($adminId)) {
    add_flash("No admin specified", FLASH_ERROR);
    redirect_to($redirectUrl);
}

$adminManager = AdminAuthManager::instance();
$admin = $adminManager->getAdmin($adminId);

if (empty($admin)) {
    add_flash("The admin does not exist", FLASH_ERROR);
    redirect_to($redirectUrl);
}

$smarty->assign('admin', $admin);

$errors = [];
if (filter_has_var(INPUT_POST, "edit_admin")) {
    $data = clean_data();
    if (validate($data)) {
        $editStatus = $adminManager->editAdmin($adminId, $data['email']
                , $data['is_super'], $data['is_active']);
        $message = $editStatus ? "Successfully edited admin" :
                "Failed to edit admin";
        add_flash($message, $editStatus ? FLASH_SUCCESS : FLASH_ERROR);
        if ($editStatus) {
            redirect_to($redirectUrl);
        }
    }
}

$smarty->assign("errors", $errors);

$smarty->display("edit_admin.tpl");

function validate($data) {
    $errors = [];
    if (empty($data['email'])) {
        $errors['email'] = "Please type in a valid email";
    }
    $GLOBALS['errors'] = $errors;
    return empty($errors);
}

function clean_data() {
    $data = [];
    $data['email'] = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL);
    $data['is_active'] = filter_input(INPUT_POST, "is_active", FILTER_SANITIZE_NUMBER_INT);
    $data['is_super'] = filter_input(INPUT_POST, "is_super", FILTER_SANITIZE_NUMBER_INT);
    return $data;
}
