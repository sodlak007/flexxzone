<?php

require_once './config.php';

$entryId = filter_input(INPUT_GET, "entry", FILTER_SANITIZE_NUMBER_INT);

$redirectUrl = $_SERVER["HTTP_REFERER"] ? : BASE_URL . "entries.php";

if(empty($entryId)){
    add_flash("Entry to be deleted not specified", FLASH_ERROR);
    redirect_to($redirectUrl);
}

$entryHelper = EntryManager::instance();

$entry = $entryHelper->get_stage_2_entry($entryId);

if(empty($entry)){
    add_flash("The entry could not be found", FLASH_ERROR);
    redirect_to($redirectUrl);
}

//first delete all entry votes
$voteDeleteStatus = $entryHelper->delete_stage_2_entry_votes($entryId);

$entryDeleteStatus = $entryHelper->delete_stage_2_entry($entryId);

if($voteDeleteStatus && $entryDeleteStatus){
    add_flash("Successfully deleted entry", FLASH_SUCCESS);
}elseif(!$voteDeleteStatus && $entryDeleteStatus){
    add_flash("Deleted entry but failed to delete its votes", FLASH_ERROR);
}elseif($voteDeleteStatus && !$entryDeleteStatus){
    add_flash("Successfully deleted entry votes but failed to delete "
            . "entries itself.", FLASH_ERROR);
}else{
    add_flash("Failed to delete entry", FLASH_ERROR);
}

redirect_to($redirectUrl);