<?php

require_once './config.php';

$adminManager = AdminAuthManager::instance();

$adminManager->logout();

redirect_to(BASE_URL);