<?php

require_once './config.php';

$adminManager = AdminAuthManager::instance();

$force = filter_input(INPUT_GET, "force", FILTER_SANITIZE_NUMBER_INT);
if($adminManager->isLoggedIn() && empty($force)){
    redirect_to(BASE_URL);
}

if(filter_has_var(INPUT_POST, "login")){
    $username = filter_input(INPUT_POST, "email", FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
    $loginStatus = $adminManager->login($username, $password);
    if($loginStatus){
        add_flash("You have been successfully logged in", FLASH_SUCCESS);
        redirect_to(BASE_URL);
    }else{
        add_flash("Wrong username or password", FLASH_ERROR);
    }
}

$smarty->display("login.tpl");