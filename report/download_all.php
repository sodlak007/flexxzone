<?php

require_once './config.php';

$entriesHelper = EntryManager::instance();

$totalItemsCount = $entriesHelper->get_entries_count();

if (empty($totalItemsCount)) {
    add_flash("No entries to download", FLASH_INFO);
    redirect_to(BASE_URL . "entries.php");
}
$filename = "flexx_your_creativity_entrants.csv";
send_headers();

//create file
$handle = fopen($filename, "w");
$iterations = ceil($totalItemsCount / 1000);

$counter = 0;

$headings = ["#", "First Name", "Last Name", "Email", "Phone Number", "Flexx Account", "Date"];
fputcsv($handle, $headings);

for ($i = 0; $i < $iterations; $i++) {
    $entries = $entriesHelper->get_entries($i, 1000);
    foreach ($entries as $entry) {
        unset($entry['id']);
        unset($entry['entry_path']);
        unset($entry['thumbnail']);
        unset($entry['status']);
        $countArr = [];
        $countArr["#"] = ++$counter;
        $fields = array_merge($countArr, $entry);
        fputcsv($handle, $fields);
    }
}
fclose($handle);
send_file($filename);
//delete file
unlink($filename);
exit();

function send_headers() {
    $filename = $GLOBALS['filename'];
    header('Content-Length:' . filesize($filename));
    header("Content-Type:text/csv");
    header('Content-Disposition:attachment; filename="' . $filename . '"');
}

function send_file($filename){
    readfile($filename);
}