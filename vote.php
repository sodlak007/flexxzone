<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once './config.php';

$entryHelper = EntryManager::instance();

$page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT) ? : 1;

//$ipp = filter_input(INPUT_GET, "ipp", FILTER_SANITIZE_NUMBER_INT) ? : 12;
$search = filter_input(INPUT_GET, "search", FILTER_SANITIZE_STRING) ? : "";
$totalItemsCount = $entryHelper->get_entries_count();
$pagination = new Pagination(BASE_URL . "vote.php", $totalItemsCount, $page);

$smarty->assign("entries", $entryHelper->get_entries(($page - 1) * $pagination->getIpp()
                , $pagination->getIpp(), $search));
$smarty->assign("pageTitle", CONTEST_NAME . " Contest Entries");
$smarty->assign("pagination", $pagination);

$smarty->display('vote.tpl');
