<?php

/* 
 * Contains autoload functions for classes.
 */

function src_autoload($className){
    $path = SRC_PATH . $className . ".php";
    if(file_exists($path)){
        require_once $path;
    }
}

spl_autoload_register("src_autoload");