<?php

/* 
 * Contains messages for the site
 */

$MESSAGES = [
    "stage_1_submission_closed" => "The Contest is now closed for submissions but you can still vote for your friend to win",
    "stage_1_voting_closed" => "Voting for this stage of the contest has been closed",
    "stage_2_submission_closed" => "Submission of entries for the second stage is now closed",
    "stage_2_voting_closed" => "Voting for the second stage of the contest has been closed",
    "stage_3_submission_closed" => "Submission of entries for the third stage of the contest has closed",
    "stage_3_voting_closed" => "Voting for this stage of the contest has been closed",
];