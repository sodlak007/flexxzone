<?php

$GENCONFIG = [
    "contest" => [
        "stage_1" => [
            "number_of_winners" => 20,
            "submission_closure_date" => "2016-08-23 23:59:59",
            "vote_closure_date" => "2016-08-26 13:00:00"
        ],
        "stage_2" => [
            "number_of_winners" => 5,
            "number_of_entrants" => 20,
            "opening_date" => "2016-08-26 00:00:00",
            "submission_closure_date" => "2016-08-30 12:00:00",
            "vote_closure_date" => "2016-08-30 23:59:59",
        ],
        "stage_3" => [
            "number_of_winners" => 1,
            "number_of_entrants" => 5,
            "opening_date" => "2016-08-31 00:00:00",
            "submission_closure_date" => "2016-09-01 23:59:59",
            "vote_closure_date" => "2016-09-02 12:00:00",
        ],
    ],
    "emails" => [
        "admin" => "oyelaking@gmail.com"
    ]
];