<?php

/*
 * Default local config. Edit values to connect to your localhost..
 */

$CONFIG = [
    "db" => [
        "host" => "localhost",
        "dbName" => "theflex1_flexgists",
        "user" => "root",
        "password" => "",
    ],
    "site" => [
        "hostname" => "localhost",
        "base_url" => "localhost",
    ],
    "emails" => [
        "admin" => "oyelaking@gmail.com"
    ],
    "facebook" => [
        "app_id" => "1759471020963499"
    ],
];
