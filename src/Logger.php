<?php

require_once BASE_PATH . 'phpmailer/class.phpmailer.php';

/**
 * Description of Logger
 *
 * @author Oyelaking
 */
class Logger {

    const LEVEL_FATAL = 8;
    const LEVEL_ERROR = 7;
    const LEVEL_WARNING = 6;
    const LEVEL_INFO = 5;

    protected static $levelToText = [
        self::LEVEL_FATAL => "FATAL",
        self::LEVEL_ERROR => "ERROR",
        self::LEVEL_INFO => "INFO",
        self::LEVEL_WARNING => "WARNING"
    ];

    public static function log($level, $message, $title = "") {
        if (!is_live() || $level > self::LEVEL_WARNING) {
            self::logToFile(self::formatMessage($level, $message));
        }
        if (is_live() && $level > self::LEVEL_WARNING) {
            self::mailError($message, $title);
        }
    }

    protected static function formatMessage($level, $message) {
        $timeFormated = date("Y-m-d H:i:s");
        return "[$timeFormated] [" . self::$levelToText[$level] . "] $message \n";
    }

    protected static function logToFile($message) {
        if (file_exists(SYSTEM_LOG) && !is_writable(SYSTEM_LOG)) {
            if (is_live()) {
                mail_error(SYSTEM_LOG . " file is not writable", "Log File Not Writable");
            } else {
                die(SYSTEM_LOG . " log file is not writable");
            }
            return;
        }
        file_put_contents(SYSTEM_LOG, $message, FILE_APPEND);
    }

    protected static function mailError($message, $title) {
        mail_error($title, $message);
    }

}
