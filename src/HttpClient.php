<?php

/**
 * HttpClient helps in making requests
 *
 * @author Oyelaking
 */
abstract class HttpClient {

    protected function makeCall($url, $data = [], $method = "POST") {
        $headers = [
            "Accept: text/plain application/json",
        ];
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        if (strtolower($method) == "post") {
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

}
