<?php

/**
 * AdminAuthManager manages admin backend.
 *
 * @author Oyelaking
 */
class AdminAuthManager {

    protected static $instance;

    const SESSION_KEY = "__admin__";
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const ACTION_DELETE_ENTRY = 1;
    const ACTION_DELETE_VOTES = 2;
    const ACTION_LOGIN = 3;
    const ACTION_LOGOUT = 4;

    protected $errors = [];

    protected function __construct() {
        
    }

    /**
     * 
     * @return AdminAuthManager
     */
    public static function instance() {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function isLoggedIn() {
        return !empty($_SESSION[self::SESSION_KEY]);
    }

    public function login($username, $password) {
        $conn = $GLOBALS['conn'];
        $passwordHash = $this->hashPassword($password);
        $query = "SELECT * FROM admin WHERE email = '$username' AND "
                . "password = '$passwordHash' and is_active = 1";
        $results = $conn->query($query);
        if (empty($results) || $results->num_rows < 1) {
            return false;
        }
        $row = $results->fetch_assoc();
        if (!empty($conn->error)) {
            Logger::log(Logger::LEVEL_ERROR, $conn->error);
            return false;
        }
        if (empty($row)) {
            return false;
        }
        $_SESSION[self::SESSION_KEY] = $row;
        return true;
    }

    public function logout() {
        unset($_SESSION[self::SESSION_KEY]);
    }

    protected function hashPassword($password) {
        return hash("sha256", $password);
    }

    public function createAdmin($data) {
        $data['is_active'] = isset($data['is_active']) ? (int) $data['is_active'] : 0;
        $data['is_super'] = isset($data['is_super']) ? (int) $data['is_super'] : 0;
        $queryFormat = "INSERT INTO admin (email, password, is_active, is_super) "
                . "VALUES('%s', '%s', %d, %d)";
        if (!$this->validateData($data)) {
            return false;
        }
        $query = sprintf($queryFormat, $data['email'], $this->hashPassword($data['password'])
                , $data['is_active'], $data['is_super']);
        $conn = $GLOBALS['conn'];
        $conn->query($query);
        if (!empty($conn->error)) {
            Logger::log(Logger::LEVEL_ERROR, $conn->error);
            return false;
        }
        return $conn->affected_rows == 1;
    }

    public function editAdmin($adminId, $email, $is_super, $is_active) {
        $queryFormat = "UPDATE admin SET email = '%s', is_super = %d, is_active = %d WHERE id = " . (int) $adminId;
        $query = sprintf($queryFormat, $email, $is_super, $is_active);
        $conn = $GLOBALS['conn'];
        $conn->query($query);
        if ($conn->error) {
            Logger::log(Logger::LEVEL_ERROR, $conn->error);
            return false;
        }
        return $conn->affected_rows > 0;
    }

    public function resetAdminPassword($adminId, $password) {
        $queryFormat = "UPDATE admin SET password = '%s' WHERE id = " . (int) $adminId;
        $query = sprintf($queryFormat, $this->hashPassword($password));
        $conn = $GLOBALS['conn'];
        $conn->query($query);
        if ($conn->error) {
            Logger::log(Logger::LEVEL_ERROR, $conn->error);
            return false;
        }
        return $conn->affected_rows == 1;
    }

    public function validateData($data) {
        if (empty($data['email'])) {
            $this->errors['email'] = "Please enter email";
        } else if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $this->errors['email'] = "Please enter a valid email address";
        }

        if (empty($data['password'])) {
            $this->errors['password'] = "Please type in a valid password";
        } else if ($data['password'] != $data['password_confirm']) {
            $this->errors['password_confirm'] = "Please type in the password again to confirm";
        } else {
            $this->validatePassword($data['password']);
        }
        return empty($this->errors);
    }

    public function validatePassword($password) {
        if (strlen($password) < 10) {
            $this->errors["password"] = "Password must be 10 or more characters";
        } else if (!preg_match("/.*\\d{2,50}.*/", $password)) {
            $this->errors["password"] = "Password must contain at least 2 numbers";
        }
        return empty($this->errors["password"]);
    }

    public function activateAdmin($id) {
        return $this->changeAdminStatus($id, self::STATUS_ACTIVE);
    }

    public function deactivateAdmin($id) {
        return $this->changeAdminStatus($id, self::STATUS_INACTIVE);
    }

    protected function changeAdminStatus($id, $status) {
        $query = "UPDATE admin SET is_active = " . (int) $status . " WHERE id = " . (int) $id;
        $conn = $GLOBALS['conn'];
        $conn->query($query);
        return $conn->affected_rows == 1;
    }

    public function isActive($id) {
        $query = "SELECT * FROM admin WHERE id = " . (int) $id . " AND is_active = 1";
        $conn = $GLOBALS['conn'];
        $results = $conn->query($query);
        if (empty($results) || $results->num_rows < 1) {
            return [];
        }
        return true;
    }

    public function getAdmins($offset, $limit, $where = "", $order = "id DESC") {
        if ($where) {
            $where = "WHERE $where";
        }
        if ($order) {
            $order = "ORDER BY $order";
        }
        $offset_e = $offset ? (int) $offset : 0;
        $limit_e = $limit ? (int) $limit : 20;
        $query = "SELECT * FROM admin $where $order LIMIT $offset_e, $limit_e";
        $conn = $GLOBALS['conn'];
        $results = $conn->query($query);
        if (!empty($conn->error)) {
            Logger::log(Logger::LEVEL_ERROR, $conn->error);
            return false;
        }
        if (empty($results) || $results->num_rows < 1) {
            return [];
        }
        $admins = [];
        while ($row = $results->fetch_assoc()) {
            $admins[] = $row;
        }
        return $admins;
    }

    public function getAdmin($id) {
        $query = "SELECT * FROM admin WHERE id = " . (int) $id;
        $conn = $GLOBALS['conn'];
        $results = $conn->query($query);
        if (!empty($conn->error)) {
            Logger::log(Logger::LEVEL_ERROR, $conn->error);
            return false;
        }
        if (empty($results) || $results->num_rows < 1) {
            return false;
        }
        return $results->fetch_assoc();
    }

    public function isLoggedAdminSuper() {
        $loggedInAdmin = $this->getLoggedInAdmin();
        if (empty($loggedInAdmin)) {
            return false;
        }
        return !empty($loggedInAdmin['is_super']);
    }

    public function getLoggedInAdmin() {
        return !empty($_SESSION[self::SESSION_KEY]) ? $_SESSION[self::SESSION_KEY] : null;
    }

    public function getErrors() {
        return $this->errors;
    }

    public function getAdminsCount($where = "") {
        $conn = $GLOBALS['conn'];
        if (!empty($where)) {
            $where = "WHERE $where";
        }
        $query = "SELECT COUNT(*) AS admin_count FROM admin $where";
        $results = $conn->query($query);
        if (!empty($conn->error)) {
            Logger::log(Logger::LEVEL_ERROR, $conn->error);
            return false;
        }
        if (empty($results) || $results->num_row = 0) {
            return [];
        }
        $adminCountRow = $row = $results->fetch_assoc();
        return $adminCountRow['admin_count'];
    }

}
