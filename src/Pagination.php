<?php

/**
 * Description of Pagination
 *
 * @author Oyelaking
 */
class Pagination {

    const DEFAULT_IPP = 12;

    private $ipp = self::DEFAULT_IPP;
    private $page = 1;
    private $totalItemsCount = 0;
    private $maxLinksDisplay = 5;
    private $pageLink = "";
    private $betweenTwoLungs = false;
    private $offset;
    private $limit;
    private $displayCount = true;
    private $showIpp = false;

    public function __construct($pageLink, $totalItemsCount = 0, $page = 1, $ipp = self::DEFAULT_IPP) {
        $this->totalItemsCount = $totalItemsCount;
        $this->page = $page;
        $this->setIpp($ipp);
        $this->pageLink = $pageLink;
        $this->init();
    }

    protected function init() {
        if ($this->page > $this->maxLinksDisplay && $this->page < $this->getNumberOfPages()) {
            $this->maxLinksDisplay = $this->maxLinksDisplay - 2;
            $this->betweenTwoLungs = true;
        }
    }

    public function getIpp() {
        return $this->ipp;
    }

    public function getPage() {
        return $this->page;
    }

    public function getTotalItemsCount() {
        return $this->totalItemsCount;
    }

    public function getMaxLinksDisplay() {
        return $this->maxLinksDisplay > $this->getNumberOfPages() ?
                $this->getNumberOfPages() : $this->maxLinksDisplay;
    }

    public function getPageLink() {
        return $this->pageLink;
    }

    public function getNextPage() {
        return $this->page + 1;
    }

    public function getPrevPage() {
        return $this->page - 1;
    }

    public function setIpp($ipp) {
        $this->ipp = $ipp;
        $this->offset = ($this->page - 1) * $this->ipp;
        $this->limit = $this->offset + $this->ipp;
        if ($this->limit >= $this->totalItemsCount) {
            $this->limit = $this->totalItemsCount;
        }
        $this->init();
        return $this;
    }

    public function setPage($page) {
        $this->page = $page;
        return $this;
    }

    public function setTotalItemsCount($totalItemsCount) {
        $this->totalItemsCount = $totalItemsCount;
        return $this;
    }

    public function setMaxLinksDisplay($maxLinksDisplay) {
        $this->maxLinksDisplay = $maxLinksDisplay;
        return $this;
    }

    public function setPageLink($pageLink) {
        $this->pageLink = $pageLink;
        return $this;
    }

    public function getNumberOfPages() {
        if ($this->totalItemsCount < 1 || $this->ipp < 0) {
            return 0;
        }
        return ceil($this->totalItemsCount / $this->ipp);
    }

    public function getPageUrl($_page) {
        $page = (int) $_page ? : 1;
        if (empty($this->pageLink)) {
            return "";
        }
        $joinChar = "?";
        if (strpos($this->pageLink, "?")) {
            $joinChar = "&";
        }
        return $this->pageLink . $joinChar . "page=$page&ipp=$this->ipp";
    }

    public function getBetweenTwoLungs() {
        return $this->betweenTwoLungs;
    }

    public function getOffset() {
        return $this->offset;
    }

    public function getLimit() {
        return $this->limit;
    }

    public function getDisplayCount() {
        return $this->displayCount;
    }

    public function setDisplayCount($displayCount) {
        $this->displayCount = $displayCount;
        return $this;
    }

    public function getShowIpp() {
        return $this->showIpp;
    }

    public function setShowIpp($showIpp) {
        $this->showIpp = $showIpp;
        return $this;
    }

}
