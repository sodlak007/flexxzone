<?php

/**
 * Description of EntryManager
 *
 * @author Oyelaking
 */
class EntryManager {

    private static $instance;
    private $errors = [];

    protected function __construct() {
        
    }

    /**
     * 
     * @return EntryManager
     */
    public static function instance() {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function get_entries($offset, $limit, $order = "", $search = "") {
        $entries = [];
        $con = $GLOBALS['conn'];
        $where = "";
        if (!empty($order)) {
            $order = "ORDER BY $order";
        }
        $query = "SELECT * FROM participant_entries $where $order LIMIT $offset, $limit";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows > 0) {
            while ($entry = $results->fetch_assoc()) {
                $entry["vote_count"] = $this->get_vote_count($entry['id']);
                $entry["thumbnail"] = str_replace(".", "_thumb.", $entry["entry_path"]);
                $entries[] = $entry;
            }
            $results->free();
        }
        return $entries;
    }

    function get_stage_2_winners($offset, $limit, $order = "", $search = "") {
        $entries = [];
        $con = $GLOBALS['conn'];
        $where = "WHERE pe.is_winner = 1";
        if (!empty($order)) {
            $order = "ORDER BY $order";
        }
        $query = "SELECT s2e.*, pe.first_name, pe.last_name, "
                . "pe.flexx_account_number, pe.phone_number, pe.email "
                . "FROM participant_entries pe left JOIN stage_2_entries "
                . "s2e on pe.id = s2e.stage_1_id $where $order LIMIT $offset, $limit";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows > 0) {
            while ($entry = $results->fetch_assoc()) {
                $entry["vote_count"] = $this->get_stage_2_vote_count($entry['stage_1_id']);
                $entry["thumbnail"] = str_replace(".", "_thumb.", $entry["entry_path"]);
                $entries[] = $entry;
            }
            $results->free();
        }
        return $entries;
    }
    
    function get_stage_2_entries($offset, $limit, $order = "", $search = "") {
        $entries = [];
        $con = $GLOBALS['conn'];
        $where = "WHERE pe.is_winner = 1";
        if (!empty($order)) {
            $order = "ORDER BY $order";
        }
        $query = "SELECT s2e.*, pe.first_name, pe.last_name, "
                . "pe.flexx_account_number, pe.phone_number, pe.email "
                . "FROM participant_entries pe INNER JOIN stage_2_entries "
                . "s2e on pe.id = s2e.stage_1_id $where $order LIMIT $offset, $limit";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows > 0) {
            while ($entry = $results->fetch_assoc()) {
                $entry["vote_count"] = $this->get_stage_2_vote_count($entry['stage_1_id']);
                $entry["thumbnail"] = str_replace(".", "_thumb.", $entry["entry_path"]);
                $entries[] = $entry;
            }
            $results->free();
        }
        return $entries;
    }

    function get_stage_3_winners($offset, $limit, $order = "", $search = "") {
        $entries = [];
        $con = $GLOBALS['conn'];
        $where = "";
        if (!empty($order)) {
            $order = "ORDER BY $order";
        }
        $query = "select * from (SELECT s2e.id as stage_2_entry_id, "
                . "pe.first_name, pe.last_name, pe.flexx_account_number, "
                . "pe.phone_number, pe.email FROM participant_entries pe "
                . "inner JOIN stage_2_entries s2e on pe.id = s2e.stage_1_id "
                . "WHERE s2e.is_winner = 1) "
                . "as entrant_info left join stage_3_entries s3e on "
                . "entrant_info.stage_2_entry_id = s3e.stage_2_id "
                . "$where $order LIMIT $offset, $limit";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows > 0) {
            while ($entry = $results->fetch_assoc()) {
                $entry["vote_count"] = $this->get_stage_3_vote_count($entry['stage_2_entry_id']);
                $entry["thumbnail"] = str_replace(".", "_thumb.", $entry["entry_path"]);
                $entries[] = $entry;
            }
            $results->free();
        }
        return $entries;
    }
    
    function get_stage_3_entries($offset, $limit, $order = "", $search = "") {
        $entries = [];
        $con = $GLOBALS['conn'];
        $where = "";
        if (!empty($order)) {
            $order = "ORDER BY $order";
        }
        $query = "select * from (SELECT s2e.id as stage_2_entry_id, "
                . "pe.first_name, pe.last_name, pe.flexx_account_number, "
                . "pe.phone_number, pe.email FROM participant_entries pe "
                . "inner JOIN stage_2_entries s2e on pe.id = s2e.stage_1_id) "
                . "as entrant_info INNER join stage_3_entries s3e on "
                . "entrant_info.stage_2_entry_id = s3e.stage_2_id "
                . "$where $order LIMIT $offset, $limit";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows > 0) {
            while ($entry = $results->fetch_assoc()) {
                $entry["vote_count"] = $this->get_stage_3_vote_count($entry['stage_2_id']);
                $entry["thumbnail"] = str_replace(".", "_thumb.", $entry["entry_path"]);
                $entries[] = $entry;
            }
            $results->free();
        }
        return $entries;
    }

    function get_vote_count($entryId) {
        $con = $GLOBALS['conn'];
        $query = "SELECT count(*) as vote_count FROM votes WHERE entry_id = $entryId";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $result = $results->fetch_assoc();
            $results->free();
            return $result['vote_count'];
        }
        return 0;
    }

    function get_stage_2_vote_count($entryId) {
        $con = $GLOBALS['conn'];
        $query = "SELECT count(*) as vote_count FROM stage_2_votes WHERE stage_1_id = $entryId";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $result = $results->fetch_assoc();
            $results->free();
            return $result['vote_count'];
        }
        return 0;
    }

    function get_stage_3_vote_count($stage2Id) {
        $con = $GLOBALS['conn'];
        $query = "SELECT count(*) as vote_count FROM stage_3_votes "
                . "WHERE stage_2_id = " . (int) $stage2Id;
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $result = $results->fetch_assoc();
            $results->free();
            return $result['vote_count'];
        }
        return 0;
    }

    function get_all_vote_count() {
        $con = $GLOBALS['conn'];
        $query = "SELECT count(*) as vote_count FROM votes";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $result = $results->fetch_assoc();
            $results->free();
            return $result['vote_count'];
        }
        return 0;
    }

    function get_all_stage_2_vote_count() {
        $con = $GLOBALS['conn'];
        $query = "SELECT count(*) as vote_count FROM stage_2_votes";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $result = $results->fetch_assoc();
            $results->free();
            return $result['vote_count'];
        }
        return 0;
    }

    function get_all_stage_3_vote_count() {
        $con = $GLOBALS['conn'];
        $query = "SELECT count(*) as vote_count FROM stage_3_votes";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $result = $results->fetch_assoc();
            $results->free();
            return $result['vote_count'];
        }
        return 0;
    }

    function entry_exists($entryId) {
        $con = $GLOBALS['conn'];
        $query = "SELECT * FROM participant_entries where id = $entryId";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $results->free();
            return true;
        }
        return false;
    }
    
    function flexx_account_number_entry_exists($flexx_account_number) {
        $con = $GLOBALS['conn'];
        $query = "SELECT * FROM participant_entries where flexx_account_number = '$flexx_account_number'";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $results->free();
            return true;
        }
        return false;
    }

    function stage_2_entry_exists($entryId) {
        $con = $GLOBALS['conn'];
        $query = "SELECT * FROM stage_2_entries where stage_1_id = " . (int) $entryId;
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $results->free();
            return true;
        }
        return false;
    }

    function stage_3_entry_exists($stage2Id) {
        $con = $GLOBALS['conn'];
        $query = "SELECT * FROM stage_3_entries where stage_2_id = " . (int) $stage2Id;
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $results->free();
            return true;
        }
        return false;
    }
    
    function stage_2_entry_id_exists($id) {
        $con = $GLOBALS['conn'];
        $query = "SELECT * FROM stage_2_entries where id = " . (int) $id;
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $results->free();
            return true;
        }
        return false;
    }

    function stage_3_entry_id_exists($id) {
        $con = $GLOBALS['conn'];
        $query = "SELECT * FROM stage_3_entries where id = " . (int) $id;
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $results->free();
            return true;
        }
        return false;
    }

    function get_entry($entryId) {
        $con = $GLOBALS['conn'];
        $query = "SELECT * FROM participant_entries where id = $entryId";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $entry = $results->fetch_assoc();
            $entry["vote_count"] = $this->get_vote_count($entry['id']);
            $entry["thumbnail"] = str_replace(".", "_thumb.", $entry["entry_path"]);
            $results->free();
            return $entry;
        }
        return false;
    }
    
    function get_entry_by_flexx_account($flexx_account) {
        $con = $GLOBALS['conn'];
        $query = "SELECT * FROM participant_entries where flexx_account_number = '$flexx_account'";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $entry = $results->fetch_assoc();
            $entry["vote_count"] = $this->get_vote_count($entry['id']);
            $entry["thumbnail"] = str_replace(".", "_thumb.", $entry["entry_path"]);
            $results->free();
            return $entry;
        }
        return false;
    }

    function get_stage_3_entry($entryId) {
        $con = $GLOBALS['conn'];
        $where = "where stage_2_id = " . (int) $entryId;
        $query = "select * from (SELECT s2e.id as stage_2_entry_id, "
                . "pe.first_name, pe.last_name, pe.flexx_account_number, "
                . "pe.phone_number, pe.email FROM participant_entries pe "
                . "inner JOIN stage_2_entries s2e on pe.id = s2e.stage_1_id) "
                . "as entrant_info INNER join stage_3_entries s3e on "
                . "entrant_info.stage_2_entry_id = s3e.stage_2_id "
                . "$where";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $entry = $results->fetch_assoc();
            $entry["vote_count"] = $this->get_vote_count($entry['id']);
            $entry["thumbnail"] = str_replace(".", "_thumb.", $entry["entry_path"]);
            $results->free();
            return $entry;
        }
        return false;
    }
    
    function get_stage_3_entry_by_id($id) {
        $con = $GLOBALS['conn'];
        $query = "SELECT * FROM stage_3_entries where id = " . (int) $id;
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $entry = $results->fetch_assoc();
            $entry["vote_count"] = $this->get_vote_count($entry['id']);
            $entry["thumbnail"] = str_replace(".", "_thumb.", $entry["entry_path"]);
            $results->free();
            return $entry;
        }
        return false;
    }

    function get_stage_2_entry($entryId) {
        $con = $GLOBALS['conn'];
        $where = "where stage_1_id = " . (int) $entryId;
        $query = "SELECT s2e.*, pe.first_name, pe.last_name, "
                . "pe.flexx_account_number, pe.phone_number, pe.email "
                . "FROM participant_entries pe INNER JOIN stage_2_entries "
                . "s2e on pe.id = s2e.stage_1_id $where";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $entry = $results->fetch_assoc();
            $entry["vote_count"] = $this->get_vote_count($entry['id']);
            $entry["thumbnail"] = str_replace(".", "_thumb.", $entry["entry_path"]);
            $results->free();
            return $entry;
        }
        return false;
    }
    
    function get_stage_2_entry_by_id($id) {
        $con = $GLOBALS['conn'];
        $query = "SELECT * FROM stage_2_entries where id = " . (int) $id;
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $entry = $results->fetch_assoc();
            $entry["vote_count"] = $this->get_vote_count($entry['id']);
            $entry["thumbnail"] = str_replace(".", "_thumb.", $entry["entry_path"]);
            $results->free();
            return $entry;
        }
        return false;
    }

    function get_entries_count() {
        $con = $GLOBALS['conn'];
        $query = "SELECT count(*) as entry_count FROM participant_entries";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $result = $results->fetch_assoc();
            $results->free();
            return $result['entry_count'];
        }
        return 0;
    }
    
    function get_stage_2_entries_count() {
        $con = $GLOBALS['conn'];
        $query = "SELECT count(*) as entry_count FROM stage_2_entries";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $result = $results->fetch_assoc();
            $results->free();
            return $result['entry_count'];
        }
        return 0;
    }
    
    function get_stage_3_entries_count() {
        $con = $GLOBALS['conn'];
        $query = "SELECT count(*) as entry_count FROM stage_3_entries";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $result = $results->fetch_assoc();
            $results->free();
            return $result['entry_count'];
        }
        return 0;
    }

    function delete_entry($entryId) {
        //delete image first
        $entry = $this->get_entry($entryId);
        if (empty($entry)) {
            return true;
        }
        unlink(BASE_PATH . $entry["entry_path"]);
        unlink(BASE_PATH . $entry["thumbnail"]); //delete thumbnail
        $sql = "DELETE FROM participant_entries WHERE id = " . (int) $entryId;
        $con = $GLOBALS['conn'];
        $con->query($sql);
        return $con->affected_rows >= 1;
    }
    
    function delete_stage_2_entry($entryId) {
        //delete image first
        $entry = $this->get_stage_2_entry($entryId);
        if (empty($entry)) {
            return true;
        }
        unlink(BASE_PATH . $entry["entry_path"]);
        unlink(BASE_PATH . $entry["thumbnail"]); //delete thumbnail
        $sql = "DELETE FROM stage_2_entries WHERE stage_1_id = " . (int) $entryId;
        $con = $GLOBALS['conn'];
        $con->query($sql);
        return $con->affected_rows >= 1;
    }
    
    function delete_stage_3_entry($stage2Id) {
        //delete image first
        $entry = $this->get_stage_3_entry($stage2Id);
        if (empty($entry)) {
            return true;
        }
        unlink(BASE_PATH . $entry["entry_path"]);
        unlink(BASE_PATH . $entry["thumbnail"]); //delete thumbnail
        $sql = "DELETE FROM stage_3_entries WHERE stage_2_id = " . (int) $stage2Id;
        $con = $GLOBALS['conn'];
        $con->query($sql);
        return $con->affected_rows >= 1;
    }

    public function delete_entry_votes($entryId) {
        $sql = "DELETE FROM votes WHERE entry_id = " . (int) $entryId;
        $con = $GLOBALS['conn'];
        $con->query($sql);
        return $con->affected_rows >= 0;
    }
    
    public function delete_stage_2_entry_votes($stage1Id) {
        $sql = "DELETE FROM stage_2_votes WHERE stage_1_id = " . (int) $stage1Id;
        $con = $GLOBALS['conn'];
        $con->query($sql);
        return $con->affected_rows >= 0;
    }
    
    public function delete_stage_3_entry_votes($stage2Id) {
        $sql = "DELETE FROM stage_3_votes WHERE stage_2_id = " . (int) $stage2Id;
        $con = $GLOBALS['conn'];
        $con->query($sql);
        return $con->affected_rows >= 0;
    }

    public function get_votes($entryId, $offset, $limit) {
        $votes = [];
        $con = $GLOBALS['conn'];
        $where = "WHERE entry_id = " . (int) $entryId;
        $query = "SELECT * FROM votes $where LIMIT $offset, $limit";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows > 0) {
            while ($vote = $results->fetch_assoc()) {
                $votes[] = $vote;
            }
            $results->free();
        }
        return $votes;
    }

    public function get_all_votes($offset, $limit) {
        $votes = [];
        $con = $GLOBALS['conn'];
        $query = "SELECT * FROM votes LIMIT $offset, $limit";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows > 0) {
            while ($vote = $results->fetch_assoc()) {
                $votes[] = $vote;
            }
            $results->free();
        }
        return $votes;
    }

    public function get_all_stage_2_votes($offset, $limit) {
        $votes = [];
        $con = $GLOBALS['conn'];
        $query = "SELECT * FROM stage_2_votes LIMIT $offset, $limit";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows > 0) {
            while ($vote = $results->fetch_assoc()) {
                $votes[] = $vote;
            }
            $results->free();
        }
        return $votes;
    }

    public function get_all_stage_3_votes($offset, $limit) {
        $votes = [];
        $con = $GLOBALS['conn'];
        $query = "SELECT * FROM stage_3_votes LIMIT $offset, $limit";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows > 0) {
            while ($vote = $results->fetch_assoc()) {
                $votes[] = $vote;
            }
            $results->free();
        }
        return $votes;
    }

    public function getEntrySelectAndVoteCountSql() {
        return $this->entrySelectAndVoteCountSql;
    }

    public function get_entries_by_count($offset, $limit, $orderDir = "desc") {
        $offset_e = (int) $offset;
        $limit_e = (int) $limit ? : 20;
        $query = "SELECT pe.*, count_t.vote_count " .
                "FROM theflex1_flexgists.participant_entries pe LEFT JOIN "
                . "(SELECT COUNT(*) AS vote_count, pe2.id AS entry_id FROM "
                . "votes INNER JOIN participant_entries pe2 ON "
                . "votes.entry_id = pe2.id GROUP BY votes.entry_id "
                . "LIMIT $offset_e, $limit_e) count_t ON pe.id = count_t.entry_id "
                . "ORDER BY vote_count $orderDir LIMIT $offset_e, $limit_e";
        $results = $GLOBALS['conn']->query($query);
        $entries = [];
        if (!empty($results) && $results->num_rows > 0) {
            while ($entry = $results->fetch_assoc()) {
                $entry["vote_count"] = $this->get_vote_count($entry['id']);
                $entry["thumbnail"] = str_replace(".", "_thumb.", $entry["entry_path"]);
                $entries[] = $entry;
            }
            $results->free();
        }
        return $entries;
    }

    public function getErrors() {
        return $this->errors;
    }

    public function promoteEntryToStage2($entryId) {
        if (empty($entryId)) {
            return false;
        }
        //check that this is a stage 1 entry
        if (!$this->entry_exists($entryId)) {
            $this->errors[] = "The stage 1 entry could not be located";
            return false;
        }
        //now check if we havent' exceeded the number of entries for the stagee
        $maxEntriesCount = $GLOBALS['CONFIG']['contest']['stage_1']['number_of_winners'];
        $entriesCount = $this->getStage2WinnersCount();
        if ($entriesCount >= $maxEntriesCount) {
            $this->errors[] = "Maximum number of winners for this stage has been reached";
            return FALSE;
        }
        //now perform an update
        $id = (int) $entryId;
        $sql = "UPDATE participant_entries SET is_winner = 1 WHERE id = $id";
        $conn = $GLOBALS['conn'];
        $conn->query($sql);
        return $conn->affected_rows == 1;
    }

    public function promoteEntryToStage3($entryId) {
        if (empty($entryId)) {
            return false;
        }
        //check that this is a stage 1 entry
        if (!$this->stage_2_entry_exists($entryId)) {
            $this->errors[] = "The stage 2 entry could not be located";
            return false;
        }
        //now check if we havent' exceeded the number of entries for the stagee
        $maxEntriesCount = $GLOBALS['CONFIG']['contest']['stage_2']['number_of_winners'];
        $entriesCount = $this->getStage2WinnersCount();
        if ($entriesCount >= $maxEntriesCount) {
            $this->errors[] = "Maximum number of winners for this stage has been reached";
            return FALSE;
        }
        //now perform an update
        $id = (int) $entryId;
        $sql = "UPDATE stage_2_entries SET is_winner = 1 WHERE stage_1_id = $id";
        $conn = $GLOBALS['conn'];
        $conn->query($sql);
        return $conn->affected_rows == 1;
    }
    
    public function removeToStage3Winner($entryId) {
        if (empty($entryId)) {
            return false;
        }
        //check that this is a stage 1 entry
        if (!$this->stage_2_entry_id_exists($entryId)) {
            $this->errors[] = "The stage 2 entry could not be located";
            return false;
        }
        //now perform an update
        $id = (int) $entryId;
        $sql = "UPDATE stage_2_entries SET is_winner = 0 WHERE id = $id";
        $conn = $GLOBALS['conn'];
        $conn->query($sql);
        return $conn->affected_rows == 1;
    }

    public function makeContestWinner($stage3Id) {
        if (empty($stage3Id)) {
            return false;
        }
        //check that this is a stage 1 entry
        if (!$this->stage_3_entry_exists($stage3Id)) {
            $this->errors[] = "The stage 3 entry could not be located";
            return false;
        }
        //now check if we havent' exceeded the number of entries for the stagee
        $maxEntriesCount = $GLOBALS['CONFIG']['contest']['stage_3']['number_of_winners'];
        $entriesCount = $this->getStage3WinnersCount();
        if ($entriesCount >= $maxEntriesCount) {
            $this->errors[] = "Maximum number of winners for this stage has been reached";
            return FALSE;
        }
        //now perform an update
        $id = (int) $stage3Id;
        $sql = "UPDATE stage_3_entries SET is_winner = 1 WHERE stage_2_id = $id";
        $conn = $GLOBALS['conn'];
        $conn->query($sql);
        return $conn->affected_rows == 1;
    }

    public function getStage1WinnersCount() {
        $con = $GLOBALS['conn'];
        $query = "SELECT count(*) as entry_count FROM participant_entries"
                . " WHERE is_winner = 1";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $result = $results->fetch_assoc();
            $results->free();
            return $result['entry_count'];
        }
        return 0;
    }

    public function getStage2WinnersCount() {
        $con = $GLOBALS['conn'];
        $query = "SELECT count(*) as entry_count FROM stage_2_entries"
                . " WHERE is_winner = 1";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $result = $results->fetch_assoc();
            $results->free();
            return $result['entry_count'];
        }
        return 0;
    }

    public function getStage3WinnersCount() {
        $con = $GLOBALS['conn'];
        $query = "SELECT count(*) as entry_count FROM stage_3_entries"
                . " WHERE is_winner = 1";
        $results = $con->query($query);
        if (!empty($results) && $results->num_rows == 1) {
            $result = $results->fetch_assoc();
            $results->free();
            return $result['entry_count'];
        }
        return 0;
    }

}
