<?php

define("CAPTCHA_SECRET", "6LfIuCcTAAAAADo0hOsYAtUSCzc4NHuEqGWmRbYt");

/**
 * Description of CaptchaValidator
 *
 * @author Oyelaking
 */
class CaptchaValidator extends HttpClient {
    
    protected $errorCodes = [
        'missing-input-secret' => "The secret parameter is missing.",
        'invalid-input-secret' => "The secret parameter is invalid or malformed.",
        'missing-input-response' => "The response parameter is missing.",
        'invalid-input-response' => "The response parameter is invalid or malformed.",
    ];
    
    protected $url = "https://www.google.com/recaptcha/api/siteverify";

    public function validateCaptcha($captchaResponse) {
        $data = [];
        $data['secret'] = CAPTCHA_SECRET;
        $data['response'] = $captchaResponse;
        $data['remoteip'] = $_SERVER["REMOTE_ADDR"];
        $response = json_decode($this->makeCall($this->url, $data), true);
        if(empty($response)){
            return false;
        }
        $status = $response['success'] == 'true';
        return $status;
    }

}
