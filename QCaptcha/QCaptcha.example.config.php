<?php


$QCaptcha = [

'width'         => 120,
'height'        => 50,
'len'           => 5,
'symbols'       => [1,2,3,4,5,6,7,8,9,'a', 'b', 'd', 'e', 'f', 'G', 'h', 'L',
                    'M', 'm', 'Q', 'q', 'R', 'r', 'T', 't'],
'onlyNumbers'   => true,
'lower'         => false,
'fontDir'       => 'fonts/',
'font'          => 'coolvetica.ttf',
'bgFont'        => 'Handylinedfont.ttf',
'type'          => 'png',
'pixelNoise'    => ['enable' => false, 'number' => 50],
'symbolNoise'   => ['enable' => false, 'number' => 18],
'filterType'    => null,
'fontSize'      => 24,
'minFontSize'   => 24,
'maxAngle'      => 30,
'minAngle'      => -30,
'wave'          => false,
'randBgColor'   => true,
'randTextColor' => true,
'textColor'     => '#ffffff',
'bgColor'       => '#000000',
'pixelColor'    => '#000000',
'whoiam'        => true

];
?>
