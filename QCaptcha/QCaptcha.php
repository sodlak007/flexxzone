<?php
/**
 * @package QCaptcha
 * @author Magalyas Dmitriy <dmitriy.d3v@gmail.com>
 * @license GNU General Public License
 * @version 0.1
 */
class QCaptcha
{
    public $width = 120;
    public $height = 50;
    public $len = 4;
    public $symbols = [1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'd', 'e', 'f', 'G',
                       'h', 'L', 'Q', 'q', 'R', 'r', 'T', 't', 'A', 'B', 'D'];
    public $onlyNumbers = false;
    public $lower = true;
    protected $image;
    public $fontDir = 'fonts/';
    public $font = 'Sketch_Block.ttf';
    public $bgFont = 'Handylinedfont.ttf';
    public $type = 'gif';
    public $pixelNoise = ['enable' => false, 'number' => 600];
    public $symbolNoise = ['enable' => true, 'number' => 25];
    public $filterType;
    public $fontSize = 24;
    public $minFontSize = 21;
    public $maxAngle = 15;
    public $minAngle = -8;
    public $wave = false;
    public $randBgColor = true;
    public $randTextColor = true;
    public $textColor = '#0f0f0f';
    public $bgColor = '#ffffff';
    public $pixelColor = '#000000';
    public $whoiam = true;
    protected $code;

    public function __construct()
    {

        try {

            session_start();
            unset($_SESSION['code']);
            if (PHP_VERSION < 5.4 || !extension_loaded('gd')) {
                throw new Exception('GD not enable or PHP version < 5.4');
            }
            $path = __DIR__ . '/' . $this->fontDir;

            if (!file_exists($path . $this->font) ||
                !file_exists($path . $this->bgFont)) {

                throw new Exception('Fonts not found');
            }

        } catch (Exception $error) {
            echo $error->getMessage();
            exit();
        }

        //Load configuration from file
        if (file_exists('QCaptcha.config.php')) {
            include 'QCaptcha.config.php';

            foreach ($QCaptcha as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    /**
     * HEX to RGB
     * @param string $hex color in HEX
     * @return array $rgb color in rgb format
     */
    protected function hex2rgb($hex)
    {
        if (strlen($hex) == 7) {
            $i = 1;
            while ($i < 6) {
                $c = substr($hex, $i, 2);
                $rgb[] = hexdec($c);
                $i += 2;
            }
            return $rgb;
        }
    }

    public function show()
    {
        $this->createImage($this->width, $this->height);
        $this->generateSymbols($this->len);

        if ($this->wave)
            $this->image = $this->wave($this->image);

        if ($this->pixelNoise['enable'])
            $this->setPixelNoise($this->pixelNoise['number']);

        if ($this->symbolNoise['enable'])
            $this->setSymbolNoise($this->symbolNoise['number']);

        if ($this->filterType != null)
            imagefilter($this->image, $this->filterType);

        if ($this->whoiam)
            $this->whoiam();

        $this->output($this->type, $this->image);
    }

    /**
     * Output image
     * @param string image type
     * @param resource image
     */
    protected function output($type, $image)
    {
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s", 10000) . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        switch ($type) {
        case 'png':
            header("Content-type: image/png");
            imagepng($image);
            break;

        case 'jpeg':
            header("Content-type: image/jpeg");
            imagejpeg($image);
            break;

        case 'gif':
            header("Content-type: image/gif");
            imagegif($image);
            break;

        default:
            header("Content-type: image/png");
            imagepng($image);
            break;
        }
        imagedestroy($image);
    }

    /**
     * The method create image
     * @param int $width
     * @param int $height
     */
    protected function createImage($width, $height)
    {
        $this->image = imagecreatetruecolor($width, $height);
        if ($this->randBgColor === true) {
            $bg = imagecolorallocate($this->image, rand(210, 240),
                                     rand(210, 240), rand(210, 240));
            imagefill($this->image, 0, 0, $bg);
        } else {
            $rbg = $this->hex2rgb($this->bgColor);
            $bg = imagecolorallocate($this->image, $rbg[0], $rbg[1], $rbg[2]);
            imagefill($this->image, 0, 0, $bg);
        }

    }

    /**
     * Method for generation symbols
     */
    protected function generateSymbols()
    {

        if ($this->randTextColor === false) {
            $rbg = $this->hex2rgb($this->textColor);
            $textColor = imagecolorallocate($this->image, $rbg[0], $rbg[1],
                                            $rbg[2]);
        }
        for ($i = 1; $i < $this->len + 1; $i++) {
            $x = $i * $this->fontSize / 1.5 + 10;
            $y = $this->height / 1.5 + rand(2, 8);
            if ($this->randTextColor === true)
                $textColor = imagecolorallocate($this->image, rand(0, 100),
                                                rand(1, 100), rand(0, 100));
            if ($this->onlyNumbers === true) {
                $symbol = rand(0, 9);
                imagettftext($this->image,
                             rand($this->minFontSize, $this->fontSize),
                             rand($this->minAngle, $this->maxAngle), $x, $y,
                             $textColor, $this->fontDir . $this->font, $symbol);
            } else {

                $symbol = $this->symbols[rand(0, sizeof($this->symbols) - 1)];
                if ($this->lower === true)
                    $symbol = mb_strtolower($symbol);
                imagettftext($this->image,
                             rand($this->minFontSize, $this->fontSize),
                             rand($this->minAngle, $this->maxAngle), $x, $y,
                             $textColor, $this->fontDir . $this->font, $symbol);
            }
            $this->code .= $symbol;
            $_SESSION['code'] = $this->code;
        }
    }
    /**
     * @param int $pexels pixel noise
     */
    protected function setPixelNoise($pixels)
    {

        $rgb = $this->hex2rgb($this->pixelColor);
        $color = imagecolorallocate($this->image, $rgb[0], $rgb[1], $rgb[2]);
        for ($i = 0; $i < $pixels; $i++) {
            imagesetpixel($this->image, rand(1, $this->width),
                          rand(1, $this->height), $color);
        }
    }

    /**
     * @param int $num noise
     */
    protected function setSymbolNoise($num)
    {
        for ($i = 0; $i < $num; $i++) {
            $textColor = imagecolorallocate($this->image, rand(100, 200),
                                            rand(100, 200), rand(100, 200));
            $symbol = rand(0, 9);
            imagettftext($this->image, rand(10, 11), rand(1, $this->maxAngle),
                         rand(1, $this->width), rand(1, $this->height),
                         $textColor, $this->fontDir . $this->bgFont, $symbol);
        }
    }

    /**
     * Method distorts the image using an algorithm wave
     * @param resource image
     */
    protected function wave($image)
    {
        //TODO: добавить сглаживание
        $newImage = imagecreatetruecolor($this->width, $this->height);
        $bg = imagecolorallocate($newImage, 255,255,255);
        imagefill($newImage, 0, 0, $bg);
        $x = 0;
        $i = 0;
        while ($x < $this->width) {
            $i = $i + 0.2; //шаг
            $y = ceil(sin($i) * 3); // смещение по Y-ку
            imagecopy($newImage, $image, $x, $y, $x, 0, 1, $this->height);
            $x++;
        }

        return $newImage;
    }

    /**
     * Just for fun =)
     */
    private function whoiam()
    {
        $color = imagecolorallocate($this->image, 0, 0, 0);
        imagefilledrectangle($this->image, 0, 0, 10, $this->height, $color);
        $textcolor = imagecolorallocate($this->image, 255, 255, 255);
        imagestringup($this->image, 0, 1, $this->height - 5, 'QCaptcha',
                      $textcolor);
        imageline($this->image, 0, 0, $this->width, 0, $color);
        imageline($this->image, 0, $this->height - 1, $this->width,
                  $this->height - 1, $color);
        imageline($this->image, $this->width - 1, 0, $this->width - 1,
                  $this->width - 1, $color);
    }
}

(new QCaptcha())->show();

?>
