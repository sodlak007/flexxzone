<?php

/*
 * Controller for the register action
 */

require_once './config.php';
require_once './securimage/securimage.php';

//check for contest closure
if(submission_closed()){
    add_flash($MESSAGES["stage_1_submission_closed"], FLASH_INFO);
    js_redirect_to(BASE_URL . "closed.php");
}

$errors = [];
$errorMessage = "";

$smarty->assignByRef("errors", $errors);
$smarty->assign("pageTitle", "Enter the " . CONTEST_NAME . " Contest");

if (filter_has_var(INPUT_POST, "apply")) {
    $cleanedData = cleanData();
    $captcha = filter_input(INPUT_POST, "captcha_code", FILTER_SANITIZE_STRING);
    if (validate($cleanedData)) {
        $securimage = new Securimage();
        if (empty($captcha)) {
            $errorMessage = "Please fill in the captcha";
        } else if (!$securimage->check($captcha)) {
            $errorMessage = "Could not validate that you are human. Please fill the captcha again";
        } else {
            $applyId = apply($cleanedData);
            if ($applyId) {
                //redirect
                redirect_to(BASE_URL . "apply-success.php?ref=$applyId");
            }
        }
    }
}

$smarty->assign("errorMessage", $errorMessage);

$smarty->display("apply.tpl");

function submission_closed(){
    $closureDate = $GLOBALS['CONFIG']['contest']['stage_1']['submission_closure_date'];
    if(strtotime($closureDate) < time()){
        return true;
    }
    return false;
}

function cleanData() {
    $data = [];
    $data['first_name'] = filter_input(INPUT_POST, "first_name", FILTER_SANITIZE_STRING);
    $data['last_name'] = filter_input(INPUT_POST, "last_name", FILTER_SANITIZE_STRING);
    $data['email'] = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
    $data['phone_number'] = filter_input(INPUT_POST, "phone_number", FILTER_SANITIZE_STRING);
    $data['flexx_account_number'] = filter_input(INPUT_POST, "flexx_account_number", FILTER_SANITIZE_STRING);
    $data['entry_image'] = $_FILES['entry_image'];
    return $data;
}

function validate($data) {
    if (empty($data['first_name'])) {
        $GLOBALS['errors']["first_name"] = "Please enter your first name";
    }
    if (empty($data['last_name'])) {
        $GLOBALS['errors']["last_name"] = "Please enter your last name";
    }
    if (empty($data['email'])) {
        $GLOBALS['errors']["email"] = "Type in a valid email address";
    } elseif (!check_unique_email($data['email'])) {// validate unique email
        $GLOBALS['errors']["email"] = "Email already exists";
        $GLOBALS['errorMessage'] = "You can not enter into this competition more than once";
    }
    if (empty($data['phone_number'])) {
        $GLOBALS['errors']["phone_number"] = "Phone number can not be empty";
    } else if (!validate_phone_number($data['phone_number'])) {
        $GLOBALS['errors']['phone_number'] = "Invadid phone number. No letters, spaces or hypens allowed";
    }
    if (empty($data['flexx_account_number'])) {
        $GLOBALS['errors']["flexx_account_number"] = "Please type in a valid Flexx account";
    }
    if (empty($data['entry_image']) || empty($data['entry_image']['tmp_name'])) {
        $GLOBALS['errors']["entry_image"] = "Please upload an image file";
    } else if (!validate_entry_image($data['entry_image'])) {
        $GLOBALS['errors']["entry_image"] = !empty($data['entry_image']['error']) ? "An error "
                . "occured while trying to upload your image entry" :
                "Please upload a valid image file not greater that 2Mb";
    }
    return empty($GLOBALS['errors']);
}

function validate_entry_image($upload) {
    return preg_match('/image\/*/', $upload['type']);
}

function apply($data) {
    //try uploading first
    $uploadPath = upload_entry($data['entry_image'], base64_encode($data['email']));
    if (empty($uploadPath)) {
        $GLOBALS['errorMessage'] = "Sign up failure. Failed to upload your image entry.";
        return false;
    }
    $sql = "INSERT INTO participant_entries (first_name, last_name, email, "
            . "phone_number, flexx_account_number, entry_path) "
            . "VALUES('%s', '%s', '%s', '%s', '%s', '%s')";
    /**
     * @var Mysqli Mysql connection instance
     */
    $con = $GLOBALS['conn'];
    $query = sprintf($sql, $data['first_name'], $data['last_name']
            , $data['email'], $data['phone_number']
            , $data['flexx_account_number'], $uploadPath);
    $con->query($query);
    if ($con->affected_rows <= 0) {
        //delete the uploaded file
        mail_error("Apply Error", "Failed to register participant entry: Sql error: " . $con->error);
        $GLOBALS["errorMessage"] = "We are sorry but your entry could not be saved.";
    } else {
        return $con->insert_id;
    }
    return false;
}

function upload_entry($entryUploadData, $fileName) {
    $nameSplitted = explode(".", $entryUploadData['name']);
    $extension = array_pop($nameSplitted);
    $uploadDir = BASE_PATH . "apply" . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR;
    $fullFileName = $fileName . ($extension ? ".$extension" : "");
    $moveSTatus = move_uploaded_file($entryUploadData['tmp_name'], $uploadDir . $fullFileName);
    if ($moveSTatus) {
        //generate thumbnail
        generate_thumbnail($fileName, $extension);
        return "apply/uploads/" . $fullFileName;
    }
    return false;
}

function check_unique_email($email) {
    $con = $GLOBALS['conn'];
    $query = "SELECT * FROM participant_entries where email = '$email'";
    $results = $con->query($query);
    if (empty($results) || $results->num_rows == 0) {
        return true;
    }
    return false;
}

function generate_thumbnail($name, $ext) {
    makeThumbnails(BASE_PATH . "apply" . DIRECTORY_SEPARATOR . "uploads"
            . DIRECTORY_SEPARATOR, "." . $ext, $name, 269, 150);
}
