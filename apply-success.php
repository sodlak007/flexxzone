<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once './config.php';

$entryId = filter_input(INPUT_GET, "ref", FILTER_SANITIZE_NUMBER_INT);

if (empty($entryId)) {
    add_flash("Invalid entry", FLASH_ERROR);
    redirect_to(BASE_URL . "apply.php");
}

$entryHelper = EntryManager::instance();

//check if entry exists
if (!$entryHelper->entry_exists($entryId)) {
    add_flash("The entry does not exist", FLASH_ERROR);
    redirect_to(BASE_URL . "apply.php");
}

$shareMessage = "I just submitted my entry. Please vote for me";

$smarty->assign("entry", $entry = $entryHelper->get_entry($entryId));
$smarty->assign("pageTitle", "Successfully Entered into the " . CONTEST_NAME . " Contest");
$smarty->assign("ogTitle", CONTEST_NAME . " Contest");
$smarty->assign("ogDesc", $shareMessage);
$smarty->assign("shareMessage", $shareMessage);
$smarty->assign("ogImage", BASE_URL . $entry['thumbnail']);

$smarty->display("apply-success.tpl");
