{extends 'layout.tpl'}

{block 'head_css'}
    <style>
        .participate p, .prize p span {
            color: #444;
            font-family: "ChaletLondonNineteenSixty", sans-serif !important;
        }
    </style>
{/block}

{block 'content'}    
    <div class="sub-header">
        <div class="row">
            <div class="large-6 medium-6 small-12 columns">
                <p>About</p>
            </div>
        </div>
    </div>

    <div class="participate">
        <div class="row">
            <div class="large-10 large-centered medium-12 small-12 columns">
                <h3 style="text-align: center;">{$smarty.const.CONTEST_NAME} Contest<span></span></h3>
                <p style="text-align: center;">Are you a fashion accessories designer aged between 16-25 years? Do you need capital to support your business? Do you want a chance to showcase your work on an acclaimed fashion stage? Then, the {$smarty.const.CONTEST_NAME} contest is just for you!</p>

                <p style="text-align: center;">Enter today for a chance to showcase your work at the Dare 2 Dream Season 3 Grand Finale (“the Grand Finale”)! Simply upload a picture of your creative design (Beads, Earrings, Bracelets, Anklets, Rings, Hair Bands etc.) and invite your friends to vote for you</p>

                <div class="divider-breaker"></div>

                <!-- <h3 style="text-align: center;">Prize<span></span></h3>
                <ul>
                    <li><p><span>1st Prize:</span> N100,000 and an opportunity to accessorise the models at the Grand Finale</p></li>
                    <li><p><span>2nd Prize:</span> N50,000 and a free ticket-for-two to the Grand Finale</p></li>
                    <li><p><span>3rd Prize:</span> N25,000 and a free ticket to the Grand Finale</p></li>
                </ul> -->

                <div class="row prize">
                    <div class="large-12 columns">
                        <h3>Prize<span></span></h3>
                    </div>

                    <div class="large-4 medium-4 small-12 columns">
                        <img src="{$BASE_URL}img/gold-medal.png">
                        <p>1st Prize:<span>N150,000 and an opportunity to accessorise the models at the Grand Finale</span></p>
                    </div>

                    <div class="large-4 medium-4 small-12 columns">
                        <img src="img/silver-medal.png">
                        <p>2nd Prize:<span>N100,000 and a free ticket-for-two to the Grand Finale</span></p>
                    </div>

                    <div class="large-4 medium-4 small-12 columns">
                        <img src="img/bronze-medal.png">
                        <p>3rd Prize:<span>N50,000 and a free ticket to the Grand Finale</span></p>
                    </div>
                </div>

                <div class="divider-breaker"></div>

                <div>
                    <center>
                        <strong>
                            <a href="{$BASE_URL}terms.php">Click here to read the Terms & Conditions</a>
                        </strong>
                    </center>
                </div>

            </div>
        </div>
    </div>

    <div class="howto">
        <div class="row">
            <div class="large-10 medium-12 large-centered small-12 columns">

                <div class="row">
                    <div class="large-12 columns">
                        <h3>How to Participate</h3>
                    </div>

                    <div class="large-4 medium-4 small-12 columns">
                        <img src="{$BASE_URL}img/mail-sent.png">
                        <p>
                            Submit your entry on the <a href="{$BASE_URL}apply.php" style="color:#F7941E;">
                                {$smarty.const.CONTEST_NAME} registration portal
                            </a>
                        </p>
                    </div>

                    <div class="large-4 medium-4 small-12 columns">
                        <img src="img/friends.png">
                        <p>Invite your friends to vote for you</p>
                    </div>

                    <div class="large-4 medium-4 small-12 columns">
                        <img src="img/trophy.png">
                        <p>Stand a chance to win if you are the contestant with the most votes.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>





{/block}