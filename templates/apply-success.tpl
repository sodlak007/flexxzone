{extends 'layout.tpl'}

{assign value="`$BASE_URL`vote_preview.php?entry=`$entry.id`" var="share_url"}

{block 'head_meta'}
    <meta property='og:url' content="{$share_url}" />
    <meta property='og:title' content="{$ogTitle}" />
    <meta property='og:description' content="{$ogDesc}" />
    <meta property='og:image' content="{$ogImage}" />
{/block}

{block 'content'}
    <div class="photoSignUp">
        <div class="row">
            <div class="large-8 large-centered medium-12 small-11 small-centered columns">
                <div class="success-box">
                    <center>
                        <img src="img/success.png">
                    </center>
                    <h3>Congratulations</h3>
                    <p>You have successfully submitted your entry  into the {$smarty.const.CONTEST_NAME} contest! Share to your friends on Facebook or Twitter and invite them to vote for you. The more votes you get, the higher your chances of winning!</p>
                    <p>
                        {assign value="`$BASE_URL`vote_preview.php?entry=`$entry.id`" var="share_url"}
                        <a target="_blank" href="{fb_share_url_2 share_url=$share_url 
redirect_url="`$BASE_URL`apply-success.php?ref=`$entry.id`"
                        title=$ogTitle image=$ogImage 
description=$ogDesc message=$shareMessage}">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>  
                        <a target="_blank" href="{twitter_share_url message="I just entered into the `$smarty.const.CONTEST_NAME` contest. Vote for me at `$BASE_URL`vote_preview.php?entry=`$entry.id`"}">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                    </p>
                    <a href="{$BASE_URL}vote.php" class="main-btn">See All Entries</a>
                </div>
            </div>
        </div>
    </div>
{/block}