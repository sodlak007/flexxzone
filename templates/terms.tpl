{extends 'layout.tpl'}

{block 'head_css'}
    <style>
        .participate p{
            color: #444;
            font-family: "ChaletLondonNineteenSixty", sans-serif;
        }
    </style>
{/block}

{block 'content'}  
    <div class="sub-header" style="margin-bottom:0;">
        <div class="row">
            <div class="large-6 medium-6 small-12 columns">
                <p>Terms & Conditions</p>
            </div>
        </div>
    </div>

    <div class="participate" style="margin-top:0px;background: #fff;">
        <div class="row">
            <div class="large-12 medium-12 small-12 columns">

                <h3 style="margin-bottom: 20px;">TERMS & CONDITIONS OF FIRST CITY MONUMENT BANK’S #FLEXXYOURCREATIVITY CONTEST (“The Contest”)<span></span></h3>

                <p><strong>General Guidelines</strong></p>
                <ul>
                    <li><p>To enter The Contest, applicant must be at least 18 years old at the time of entry</p></li>
                    <li><p>The Contest is not open to employees of First City Monument Bank (“FCMB”) or members of their immediate families</p></li>
                    <li><p>FCMB’s decision in respect of all matters to do with the competition will be final and binding on all participants</p></li>
                    <li><p>FCMB does not assume responsibility for accommodation, transportation or logistics at any point during The Contest. Participants and Winners are responsible for transporting themselves to and from the venue of the Dare2Dream Grand Finale in Lagos as needed</p></li>
                    <li><p>Prizes won during The Contest are not transferable to another person</p></li>
                    <li><p>Tickets to the Dare2Dream Grand Finale (“The Grand Finale”) which form part of the rewards for The Contest are not exchangeable for cash</p></li>
                    <li><p>The Contest deadlines including entry submission and voting deadlines are as indicated on different banners and promotional materials</p></li>
                    <li><p>FCMB reserves the right use all creative assets including participant pictures, videos, graphics and other material related to The Contest in promotion of its Flexx product.</p></li>
                    <li><p>By entering this competition, participants agree to be bound by these terms and conditions.</p></li>
                </ul>


                <p><strong>Prizes</strong></p>
                <ul>
                    <li><p><strong>First prize:</strong> N150,000 and the opportunity to accessorize the models at The Grand Finale</p></li>
                    <li><p><strong>Second prize:</strong> N100,000 and a ticket-for-two to the Grand Finale</p></li>
                    <li><p><strong>Third prize:</strong> N50,000 and a ticket-for-two to the Grand Finale</p></li>
                </ul>

                <p><strong>Entry Submissions</strong></p>
                <ul>
                    <li><p>Entry into The Contest shall be strictly through the FCMB Flexx microsite <a href="{$BASE_URL}">http://flexxzone.fcmb.com/</a> </p></li>
                    <li><p>Only Flexx account holders are eligible to participate in The Contest. Any entry discovered to have been submitted by a non-Flexx account holder will be immediately disqualified at whatever point during The Contest this discovery is made</p></li>
                    <li>
                        <p>
                            All contestants moving to the next stage will be contacted by organizers to provide the next piece. New submissions for Stages 2-3 are required within 24 hours after voting for the previous stage ends (for instance, if voting at Stage 2 ends on a Monday, contestants moving to Stage 3 will be required to submit their new entries by the following day, which is Tuesday).
                        </p>
                    </li>
                    <li><p>All entries submitted are expected to be created directly by the contestants. Submission of pictures of accessories created by someone else or purchased are strictly prohibited. Random checks will be carried out on participants at various times during the course of the event and may include invitation to a venue to demonstrate their skills in person. Any contestant discovered to have submitted an entry that cannot be replicated during the random check will be immediately disqualified at whatever point through the course of The Contest this discovery is made</p></li>
                    <li><p>Entry submission closes on Tuesday, August 23, 2016</p></li>                   
                </ul>


                <p><strong>Elimination</strong></p>

                <ul>
                    <li><p>The Contest comprises three (3) distinct stages:</p></li>
                    <li><p><strong>Stage 1 (Entry submission):</strong> This will last for 5 days and all contestants will be required to upload one picture of the accessory they created as an entry into The Contest. Once a contestant submits an entry, members of the public can immediately start voting for that entry. The 20 entries with the most votes at the end of the Entry submission window will move to Stage 2.</p></li>

                    

                    <li><p><strong>Stage 2 (Semi-finals):</strong> Winners from Stage 1 will be required to submit another piece, which will again be open to public voting for 2 days. The 5 entries with the most votes will move to </p></li>

                    <li><p><strong>Stage 3 (Finals):</strong> Winners from Stage 2 will be required to submit a final piece. They may also be required to submit a short video showing them at work or attend an onsite creative session in Lagos or any such method as may be devised to ensure they are not passing off someone else’s work as theirs. The selection of the winner at this stage will be both by a panel of judges and public voting, with the latter’s decision holding the greatest weight.</p></li>
                </ul>

                <p><strong>Voting</strong></p>

                <ul>
                    <li><p>Members of the public can vote for their preferred entries once everyday</p></li>
                    <li><p>5 tickets to the Grand Finale will be given out to members of the voting public. Winners of this ticket will be those who correctly answer the random questions about FCMB Flexx, which will be asked during the course of The Contest    </p></li>
                </ul>
            </div>        
        </div>
    </div>

{/block}