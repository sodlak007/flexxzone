{extends 'layout.tpl'}

{assign value="`$BASE_URL`vote_preview.php?entry=`$entry.id`" var="share_url"}

{block 'head_meta'}
    {if !empty($entry)}
        <meta property='og:url' content="{$share_url}" />
        <meta property='og:title' content="{$ogTitle}" />
        <meta property='og:description' content='{$ogDesc}' />
        <meta property='og:image' content="{$ogImage}" />
    {/if}
{/block}

{block 'content'}
    <div class="sub-header">
        <div class="row">
            <div class="large-6 medium-6 small-12 columns">
                <p>{$entry.first_name|capitalize} {$entry.last_name|capitalize}</p>
            </div>
        </div>
    </div>

    <div class="bigShot">
        <div class="row">
            <div class="small-12 columns">
                {if !empty($voteStatus)}
                    <div class="field-success">
                        Thanks for Voting. Click the share link below to tell your friends on Facebook and Twitter
                    </div>
                {/if}
                {if !empty($errorMessage)}
                    <div class="field-error">
                        {$errorMessage}
                    </div>
                {/if}
                <div class="big-image-ctn">
                    <img src="{$BASE_URL}{$entry.entry_path}">
                </div>
            </div>

            <div class="small-12 columns">
                {if empty($voteStatus) && empty($voteId) && !$is_voting_closed}
                    <center>
                        <form method="post">
                            <input type="text" value="{$smarty.post.voter_email|default:''}"
                                   name="voter_email" placeholder="Enter Email">
                            <input type="submit" name="vote" value="Vote">
                        </form>
                    </center>
                {/if}
            </div>

            {if !empty($voteStatus) || !empty($voteId) && !$is_voting_closed}
                <div class="small-12 columns">
                    <center>
                        <p class="social-share">
                            Share:&nbsp;
                            <a target="_blank" href="{fb_share_url_2 share_url=$share_url 
redirect_url="`$share_url`&vote=`$voteId`" title=$ogTitle image=$ogImage 
description=$ogDesc message=$shareMessage}">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>  
                            <a target="_blank" href="{twitter_share_url message=$shareMessage}">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                        </p>


                    </center>
                </div>
            {/if}
        </div>
    </div>

    <div class="howto">
        <div class="row">
            <div class="large-10 medium-12 large-centered small-12 columns">

                <div class="row">
                    <div class="large-12 columns">
                        <h3>How to Participate</h3>
                    </div>

                    <div class="large-4 medium-4 small-12 columns">
                        <img src="img/mail-sent.png">
                        <p>Submit your entry on the <a href="{$BASE_URL}apply.php" style="color:#F7941E;">
                                {$smarty.const.CONTEST_NAME} registration portal
                            </a>
                        </p>
                    </div>

                    <div class="large-4 medium-4 small-12 columns">
                        <img src="img/friends.png">
                        <p>Invite your friends to vote for you</p>
                    </div>

                    <div class="large-4 medium-4 small-12 columns">
                        <img src="img/trophy.png">
                        <p>Stand a chance to win if you are the contestant with the most votes.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}