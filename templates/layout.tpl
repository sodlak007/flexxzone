<!doctype html>
<html class="no-js" lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        {block 'head_meta'}

        {/block}
        <title>{$pageTitle|default:$CONTEST_NAME}</title>
        <link rel="stylesheet" href="css/foundation.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/app.css">

        {block 'head_css'}

        {/block}

        {block 'head_js'}

        {/block}

        {literal}
            <!-- For Google Analytics -->
            <script>
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

                ga('create', 'UA-82428607-1', 'auto');
                ga('send', 'pageview');

            </script>

            <!-- Facebook Pixel Code -->
            <script>
                !function (f, b, e, v, n, t, s) {
                    if (f.fbq)
                        return;
                    n = f.fbq = function () {
                        n.callMethod ?
                                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                    };
                    if (!f._fbq)
                        f._fbq = n;
                    n.push = n;
                    n.loaded = !0;
                    n.version = '2.0';
                    n.queue = [];
                    t = b.createElement(e);
                    t.async = !0;
                    t.src = v;
                    s = b.getElementsByTagName(e)[0];
                    s.parentNode.insertBefore(t, s)
                }(window,
                        document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

                fbq('init', '164037387360331');
                fbq('track', "PageView");
            </script>
            <noscript>
        <img height="1" width="1" style="display:none"
             src="https://www.facebook.com/tr?id=164037387360331&ev=PageView&noscript=1" />
        </noscript>
        <!-- End Facebook Pixel Code -->
    {/literal}
</head>
<body >
    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '{$config.facebook.app_id}',
                xfbml: true,
                version: 'v2.7'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <div class="dark-header">
        <div class="row">
            <div class="large-4 medium-4 small-6 columns">
                <a href="http://flexxzone.fcmb.com"> <img src="img/logo.png" width="200" height="50"></a>
            </div>

            <div class="large-8 medium-8 small-6 columns">
                <ul>
                    <li><a href="{$BASE_URL}about.php">About</a></li>
                    <li>
                        <a href="#" class="dropdown">Entries</a>
                        <ul>
                            <li><a href="{$BASE_URL}vote.php">Stage 1</a></li>
                            <li><a href="{$BASE_URL}vote_stage_2.php">Stage 2</a></li>
                            <li><a href="{$BASE_URL}vote_stage_3.php">Stage 3</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Enter Next Stage</a>
                        <ul>
                            {if $is_stage_2}
                                <li><a href="{$BASE_URL}apply_for_stage_2.php">Stage 2</a></li>
                            {/if}            
                            {if $is_stage_3}
                                <li><a href="{$BASE_URL}apply_for_stage_3.php">Stage 3</a></li>
                            {/if}
                        </ul>
                    </li>
                </ul>
                <i class="fa fa-bars" aria-hidden="true"></i>
            </div>
        </div>
    </div>

    <div class="mobile-header">
        <a href="{$BASE_URL}about.php">About</a>        
        <a href="#" class="mobile-dropdown">Entries</a>
        <div class="stages">
            <a href="{$BASE_URL}vote.php">Stage 1</a>
            <a href="{$BASE_URL}vote_stage_2.php">Stage 2</a>
            <a href="{$BASE_URL}vote_stage_3.php">Stage 3</a>
        </div>
        <a href="#" class="mobile-dropdown">Enter Next Stage</a>
        <div class="stages">
            {if $is_stage_2}
                <a href="{$BASE_URL}apply_for_stage_2.php">Stage 2</a>
            {/if}
            {if $is_stage_3}
                <a href="{$BASE_URL}apply_for_stage_3.php">Stage 3</a>
            {/if}
        </div>
    </div>

    <div>
        {if !empty($flash.error)}
            {foreach from=$flash.error item="fl_error"}
                <div class="vote-error">
                    {$fl_error}
                </div>
            {/foreach}
        {/if}
        {if !empty($flash.info)}
            {foreach from=$flash.info item="fl_info"}
                <div class="vote-warning">
                    {$fl_info}
                </div>
            {/foreach}
        {/if}
        {if !empty($flash.success)}
            {foreach from=$flash.success item="fl_success"}
                <div class="vote-success">
                    {$fl_success}
                </div>
            {/foreach}
        {/if}
    </div>

    {block name="content"}

    {/block}

    <div class="footer">
        <div class="row">
            <div class="large-4 medium-12 small-12 columns">
                <p>© 2016 FCMB. All Rights Reserved.</p>
            </div>
            <div class="large-4 medium-12 small-12 columns">
                <div class="footer-links">
                    <a href="http://www.fcmb.com/sitemap/privacy-policy">Privacy Policy</a>
                    <a href="http://www.fcmb.com/">Visit FCMB</a>
                </div>
            </div>
            <div class="large-4 medium-12 small-12 columns">  
                <div class="social">
                    <ul>
                        <li><a href="https://www.facebook.com/fcmbmybank/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="https://www.twitter.com/myfcmb/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="https://www.instagram.com/myfcmb/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        <li><a href="https://www.youtube.com/user/fcmbplc" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
</body>
</html>
