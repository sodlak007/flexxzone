{capture assign="displayCountString"}
    <div class="small-12 columns">
        <div style="margin-top: 5px">
            <center>
                <span>
                    Showing {$pagination->getOffset() + 1} to {$pagination->getLimit()} of {$pagination->getTotalItemsCount()}
                </span>
            </center>
        </div>
    </div>
{/capture}

{if !empty($pagination) && $pagination->getNumberOfPages() > 1}
    <div class="small-12 columns">
        <div class="paginate">
            <center>
                {if $pagination->getNumberOfPages() > 3 && $pagination->getPage() != 1}
                    <a href="{$pagination->getPageUrl($pagination->getPrevPage())}"><<</a>
                {/if}

                {for $paginatorPage=1 to $pagination->getMaxLinksDisplay()}
                    <a href="{$pagination->getPageUrl({$paginatorPage})}"
                       {if $paginatorPage == $pagination->getPage()}class="current-page"{/if}>
                        {$paginatorPage}
                    </a>
                {/for}

                {if $pagination->getNumberOfpages() > ($pagination->getMaxLinksDisplay())}
                    <span class="ellipsis-page">
                        ...
                    </span>

                    {if $pagination->getBetweenTwoLungs()}
                        <a href="{$pagination->getPageUrl($pagination->getPage())}"
                           class="current-page">
                            {$pagination->getPage()}
                        </a>
                        {if ($pagination->getPage() + 1) < $pagination->getNumberOfPages()}
                            <span class="ellipsis-page">
                                ...
                            </span>
                        {/if}
                    {/if}

                    <a href="{$pagination->getPageUrl({$pagination->getNumberOfPages()})}"
                       {if $pagination->getNumberOfPages() == $pagination->getPage()}class="current-page"{/if}>
                        {$pagination->getNumberOfPages()}
                    </a>
                {/if}

                {if $pagination->getNumberOfPages() > 3 && $pagination->getPage() != $pagination->getNumberOfPages()}
                    <a href="{$pagination->getPageUrl($pagination->getNextPage())}">>></a>
                {/if}
            </center>
        </div>
        {if $pagination->getDisplayCount()}
            {$displayCountString}
        {/if}
        {if $pagination->getShowIpp()}
            <div class="small-12 columns">
                <div style="margin-top: 5px">
                    <center>
                        <span>
                            <select name="ipp" id='ippSelect' style='width: auto'>
                                <option>Items Per Page</option>
                                <option value="20" {if $pagination->getIpp() == 20}selected='selected'{/if}>20</option>
                                <option value="50" {if $pagination->getIpp() == 50}selected='selected'{/if}>50</option>
                                <option value="100" {if $pagination->getIpp() == 100}selected='selected'{/if}>100</option>
                                <option value="200" {if $pagination->getIpp() == 200}selected='selected'{/if}>200</option>
                                <option value="500" {if $pagination->getIpp() == 500}selected='selected'{/if}>500</option>
                                <option value='1000' {if $pagination->getIpp() == 1000}selected='selected'{/if}>1000</option>
                            </select>
                        </span>
                    </center>
                </div>
            </div>
            <script>
                window.addEventListener('load', ippSelect, false);
                function ippSelect() {
                    $('#ippSelect').on('change', function () {
                        var selectedIpp = $(this).val();
                        var currentUrl = window.location.href;
                        var newUrl = "";
                        if (currentUrl.indexOf("?") < 0) {
                            newUrl = currentUrl + "?page=1&ipp=" + selectedIpp;
                        } else {
                            var currentUrlSplitted = currentUrl.split("?");
                            var querySplitted = currentUrlSplitted[1].split("&");
                            var queryObj = {};
                            for (var i = 0; i < querySplitted.length; i++) {
                                var queryParts = querySplitted[i].split("=");
                                var name = queryParts[0];
                                var value = queryParts[1];
                                queryObj[name] = value;
                            }
                            queryObj.ipp = selectedIpp;
                            queryObj.page = 1;
                            newUrl = currentUrlSplitted[0] + "?";
                            var newUrlParts = [];
                            for (var name in queryObj) {
                                newUrlParts.push(name + "=" + queryObj[name]);
                            }
                            newUrl += newUrlParts.join("&");
                        }
                        window.location = newUrl;
                    });
                }
            </script>
        {/if}
    </div>
{elseif !empty($pagination)}    
    {if $pagination->getDisplayCount()}
        {$displayCountString}
    {/if}
{/if}