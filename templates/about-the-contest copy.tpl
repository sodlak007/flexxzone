{extends 'layout.tpl'}

{block 'content'}    
    <div class="sub-header">
        <div class="row">
            <div class="large-6 medium-6 small-12 columns">
                <p>About</p>
            </div>
        </div>
    </div>

    <div class="participate">
        <div class="row">
            <div class="large-12 medium-12 small-12 columns">
                <h3>{$smarty.const.CONTEST_NAME} Contest<span></span></h3>
                <p>Are you a fashion accessories designer aged between 16-25 years? Do you need capital to support your business? Do you want a chance to showcase your work on an acclaimed fashion stage? Then, the {$smarty.const.CONTEST_NAME} contest is just for you!</p>

                <p>Enter today for a chance to showcase your work at the Dare 2 Dream Season 3 Grand Finale (“the Grand Finale”)! Simply upload pictures of your creative design (Beads, Earrings, Bracelets, Anklets, Rings, Hair Bands etc.) and invite your friends to vote for you</p>

                <h3>Prize<span></span></h3>
                <ul>
                    <li><p><span>1st Prize:</span> N100,000 and an opportunity to accessorise the models at the Grand Finale</p></li>
                    <li><p><span>2nd Prize:</span> N50,000 and a free ticket-for-two to the Grand Finale</p></li>
                    <li><p><span>3rd Prize:</span> N25,000 and a free ticket to the Grand Finale</p></li>
                </ul>

                <h3>How to Participate<span></span></h3>
                <ul>
                    <li><p>Submit your entry on the {$smarty.const.CONTEST_NAME} registration portal</p></li>
                    <li><p>Invite your friends to vote for you</p></li>
                    <li><p>Stand a chance to win if you are the contestant with the most votes</p></li>
                </ul>

                <h3>Terms and Conditions<span></span></h3>
                <ul>
                    <li><p>The {$smarty.const.CONTEST_NAME} Contest is organised by First City Monument Bank</p></li>
                    <li><p>The contest will hold between 12th August 2016 and 31st August 2016</p></li>
                    <li><p> Only FCMB Flexx Account Holders are eligible to participate in this contest. If you do not already have a Flexx account, click <a href="#">here</a> to open one now</p></li>
                    <li><p>The contest comprises three stages:</p></li>
                    <li><p>Stage 1: 20 Winners will emerge in the first stage, based on the highest public votes received</p></li>
                    <li><p>Stage 2: 5 Winners will emerge in the second stage, based on the highest public votes received</p></li>
                    <li><p>Stage 3: The Grand Winner will emerge based on a combination of votes from the public and a panel of judges</p></li>
                    <li><p>All decisions taken are final and FCMB will not enter into discussions with any participant over the outcome of the contest.</p></li>
                </ul>
            </div>
        </div>
    </div>



    <div class="howto">
        <div class="row">
            <div class="large-10 medium-12 large-centered small-12 columns">

                <div class="row">
                    <div class="large-12 columns">
                        <h3>How to Participate</h3>
                    </div>

                    <div class="large-4 columns">
                        <img src="{$BASE_URL}img/mail-sent.png">
                        <p>Submit your entry on the {$smarty.const.CONTEST_NAME} registration portal</p>
                    </div>

                    <div class="large-4 columns">
                        <img src="img/friends.png">
                        <p>Invite your friends to vote for you</p>
                    </div>

                    <div class="large-4 columns">
                        <img src="img/trophy.png">
                        <p>Stand a chance to win if you are the contestant with the most votes.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}