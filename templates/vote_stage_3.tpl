{extends 'layout.tpl'}

{block 'head_css'}
    <style>
        .photobox .image{
            min-height: 150px;
            max-height: 150px;
        }
    </style>
{/block}

{block 'content'}
    <div class="banner-space" style="padding:30px 0 0;">
        <div class="row">
            <div class="large-10 large-centered medium-12 small-12 columns">
                <center>
                    <a href="http://flexxzone.fcmb.com/join/open-account.html" target="_blank">
                        <img src="{$BASE_URL}img/homepage_top_banner.png" style="border: 1px solid #ccc;">
                    </a>
                </center>
            </div>
        </div>
    </div>


    <div class="shots">
        <div class="row">
            <div class="small-12 columns">
                <h2>Stage 3 Photo Entries</h2>
            </div>

            <div class="small-12 columns">

                <!-- 
                <div class="empty-booth">
                    <center>
                        <img src="img/empty.png" width="87" height="81">
                       <p>Be the first to showcase your talent</p>
                       <a href="{$BASE_URL}apply.php"><i class="fa fa-camera-retro" aria-hidden="true"></i>Submit Your Entry</a>
                     </center>
                   </div>

                -->


                {foreach from=$entries item="entry"}
                    <div class="photobox-ctn">
                        <div class="photobox">
                            <div class="image">
                                <a href="{$BASE_URL}vote_preview_stage_3.php?entry={$entry.stage_2_id}">
                                    <img src="{$BASE_URL}{$entry.thumbnail}">
                                </a>
                                <a href="{$BASE_URL}vote_preview_stage_3.php?entry={$entry.stage_2_id}">
                                    <span>
                                        <i class="fa fa-heart" aria-hidden="true"></i>Vote
                                    </span>
                                </a>
                            </div>
                            <p>{$entry.first_name|capitalize} {$entry.last_name|capitalize}</p>
                            <span>
                                <i class="fa fa-trophy" aria-hidden="true"></i> {$entry.vote_count} vote(s)
                            </span>
                        </div>
                    </div>
                {/foreach}
            </div>

            {paginate object=$pagination}

        </div>
    </div>

    <div class="howto">
        <div class="row">
            <div class="large-10 medium-12 large-centered small-12 columns">

                <div class="row">
                    <div class="large-12 columns">
                        <h3>How to Participate</h3>
                    </div>

                    <div class="large-4 medium-4 small-12 columns">
                        <img src="img/mail-sent.png">
                        <p>Submit your entry on the <a href="{$BASE_URL}apply.php" style="color: #F7941E;">
                                {$smarty.const.CONTEST_NAME} registration portal
                            </a>
                        </p>
                    </div>

                    <div class="large-4 medium-4 small-12 columns">
                        <img src="img/friends.png">
                        <p>Invite your friends to vote for you</p>
                    </div>

                    <div class="large-4 medium-4 small-12 columns">
                        <img src="img/trophy.png">
                        <p>Stand a chance to win if you are the contestant with the most votes.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}