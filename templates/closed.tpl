{extends 'layout.tpl'}s

{block 'content'}
    <div class="photoSignUp">
        <div class="row">
            <div class="large-8 large-centered medium-12 small-11 small-centered columns">
                <div class="success-box">
                    <center>
                        <img src="img/closed.png" style="margin-bottom: 30px;">
                    </center>
                    <p>The Contest is now closed for submissions but you can still vote for your friend to win</p>
                   
                    <a href="{$BASE_URL}vote.php" class="main-btn">See All Entries</a>
                </div>
            </div>
        </div>
    </div>
{/block}