{extends 'layout.tpl'}

{block 'head_css'}
    <style>
        #applyForm div.large-6{
            min-height: 108px;
        }
    </style>
{/block}

{block 'head_js'}
    <script src='https://www.google.com/recaptcha/api.js'></script>
{/block}

{block name="content"}
    <div class="photoSignUp">
        <div class="row">
            <div class="large-8 large-centered medium-12 small-11 small-centered columns">
                <h1>Upload Your Entry For The #FlexxYourCreativity Contest Third Stage</h1>

                {if $errorMessage}
                    <div class="field-error">
                        {$errorMessage}
                    </div>
                {/if}

                <div class="field-warning">*All fields are important</div>

                <div class="row">                    
                    <form method="post" enctype="multipart/form-data" id='applyForm'
                          action="{$BASE_URL}apply_for_stage_3.php">
                        <div class="large-6 medium-6 small-12 columns">
                            <input type="text" name="flexx_account_number" 
                                   value="{$smarty.post.flexx_account_number|default:''}" placeholder="Flexx Account No">
                            {if !empty($errors.flexx_account_number)}
                                <span class="error">{$errors.flexx_account_number}</span>
                            {/if}
                            <span class="help">Don't have a Flexx Account? 
                                Open one <a href="http://flexxzone.fcmb.com/join/open-account.html">here</a></span>
                        </div>

                        <div class="large-6 medium-6 small-12 columns">
                            <div class="file">
                                <input type="hidden" name="MAX_FILE_SIZE" value="{$MAX_UPLOAD_SIZE}" />
                                <input type="file" name="entry_image" id="id_media1">
                                <span class="value"></span>
                                <span class="bt-value btn torq-btn">Upload a picture of your work</span>
                            </div>
                            {if !empty($errors.entry_image)}
                                <span class="error">{$errors.entry_image}</span>
                            {/if}
                        </div>

                        <div class="large-12 medium-12 small-12 columns">
                            <center>
                                <img id="captcha" src="{$BASE_URL}securimage/securimage_show.php" alt="CAPTCHA Image" />
                                <input type="text" name="captcha_code" size="10" maxlength="6" 
                                       style="width: auto; padding: 10px; margin-top: 10px" />
                                <a href="#" onclick="document.getElementById('captcha').src = '{$BASE_URL}/securimage/securimage_show.php?' + Math.random();
                                        return false">[ Different Image ]</a>
                            </center>
                        </div>

                        <div class="small-12 columns">
                            <center>
                                <input type="submit" name="apply" value="Enter Contest">
                            </center>
                        </div>
                    </form>
                </div>


                <div class="row">
                    <div class="large-6 large-centered medium-6 small-12 columns">
                        <p class="toas">By signing up, you agree to our <a href="{$BASE_URL}terms.php">Terms and conditions</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}